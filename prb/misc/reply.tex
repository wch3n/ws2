\documentclass[11pt,english,numbers=endperiod,parskip=full]{scrartcl}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[version=4]{mhchem}
\usepackage[breaklinks,colorlinks,linkcolor={blue},
            citecolor={blue},urlcolor={blue}]{hyperref}
\usepackage{setspace}
\onehalfspacing

\begin{document}
Dear Editor,

Thank you for having our manuscript ``Nonunique fraction of Fock exchange for defects in two-dimensional materials''
reviewed by three of your referees.
We are grateful for their valuable comments.
The first referee raises a series of comments mostly on the technical details of our methods and results.
The second referee is positive about our results but concerns about the presentation and the generality of our findings
for the study of defect physics in 2D materials.
The third referee finds our work ``of considerable interest to the condensed physics community and to the readers of PRB''.
Here, we reply to all the comments and indicate the changes that are
incorporated into the revised manuscript.

We are confident that our revised manuscript has addressed all the points raised by the referees.
We thank you for your consideration of publishing our work as a Letter in Phys.\ Rev.\ B.

Yours sincerely, \\
\smallskip \\
W. Chen, S. Griffin, G.-M. Rignanese, and G. Hautier

\subsection*{Response to Referee 1}
We thank the referee for the constructive comments.

\textit{1. For all defects considered, apparently structural relaxations are 
performed at the PBE level for the neutral charge state and used for 
all subsequent calculations (although this is not explicitly stated in 
the text). As the authors no doubt know, relaxations can have a 
profound effect on defect properties, and it is well-known that these 
are not accurately captured by semilocal functionals, especially for 
deep defects. Clearly the authors have used a simplification to test 
for Koopmans’ compliance. But the issue is that in an actual 
predictive calculation, a tuned PBE0 or HSE functional would be used 
which accounts for charge-dependent relaxations. Without considering 
this point, the authors cannot claim that a tuned functional will 
``lead to qualitatively incorrect defect physics'' as is stated in the 
abstract.}

Thank you for the comment and it was indeed not very clear from the text whether structural relaxations 
had been performed with PBE or hybrid functionals.
In fact, to determine the fulfillment of Koopmans' condition at a specific
mixing parameter, a full structural relaxation is performed at this given
mixing parameter for the charged state. 
The relaxed structure is then used for the neutral charge state at this 
specific mixing parameter.
The structures are therefore not fixed at the PBE level but are adapted to 
the amount of Fock exchange as the mixing parameter varies.
In the revised manuscript, we describe how the defect structures are relaxed 
on p.~2.

\textit{2. In order to determine $\alpha_G$ for \ce{WS2}, the authors compare with a 
$G_0W_0$ functional that does not include SOC. Why is this the comparison 
used, instead of comparing to experiment? Although when PBE0 with 
$\alpha_G$ and SOC is then used to give a band gap of 2.58 eV, there are 
experimental reports which place the band gap of \ce{WS2} at a much lower 
energy (2.4 eV in PRL 113, 076802). The chosen approach seems to bias 
in favor of a large $\alpha_G$.}

The experimental band gaps of the monolayer \ce{WS2} indeed show some scattering.
In addition to 2.5 eV (Schuler, Ref.~27) and 2.4 eV (Chernikov, PRL 113, 076802),
there is reported band gap of 2.73 eV (Zhu, Sci.\ Rep.\ 5, 1-5).
Apart from the different characterizations methods, one major cause of the scattered
experimental band gaps is the screening by the supporting substrate. 
For instance, the band gap on the monolayer \ce{MoS2} can be reduced by 
0.1-0.2~eV in the presence of \ce{SiO2} substrate (Naik, PRMater 2, 084002; Zibouche, PRB 103, 125401).
As such, the band gap reported by Chernikov could be underestimated as 
a result of the \ce{SiO2}/Si substrate used in their work.
Given the scattered experimental values and substrate effect, 
we therefore rely on the highly accurate many-body perturbation theory.
The $G_0W_0$+SOC band gap of 2.58~eV agrees well with the experimental values if the 
substrate effect is taken into account.

Additionally, in Fig.~2(a) we compare the theoretical defect levels obtained with different theories.
By aligning the PBE0 band gap with the $G_0W_0$ reference, we 
preclude any effect that stems from the use of different band gaps.

We justify the use of the $G_0W_0$ reference on p.~2 of the revised manuscript.

\textit{3. Following this point, $G_0W_0$ is used for comparison to tuned PBE0 
calculations, including for band-edge positions. Do the $G_0W_0$ band-edge 
positions agree with experiment for either \ce{WS2} or \textit{h}-BN?}

We are not aware of any experimental measurement of the band-edge positions for these two materials.
Generally speaking, $G_0W_0$ predicts the band-edge positions of semiconductors
reasonably well. For example, a survey of ionization potentials shows that $G_0W_0$ 
achieves a mean absolute error of 0.3~eV (Chen PRB 90, 165133). 
We expect such level of accuracy to be applicable to \ce{WS2} and \textit{h}-BN as well.

\textit{4. Although SOC is used to justify the choice of $\alpha_G$, SOC is 
subsequently not used in the calculation of defect properties in Fig.\ 1. 
This makes Fig.\ 1 an apples-to-oranges comparison. This bias is 
compounded by the fact that the band edges apparently do include SOC 
(at least, it appears that the gap equals $\sim 2.58$ eV at $\alpha = 0.22$). 
It seems quite possible that, if SOC was include, $\alpha_K$ could 
actually take on different values than what is determined in Fig.\ 1. }

In Table~S1 of the revised Supplemental Material (SM), we provide the single-particle eigenvalues
of the pertinent defect levels with SOC taken into account. 
Table~S1 shows that the Koopmans' condition is still largely satisfied for all the defects
considered in this work even though the $\alpha_\text{K}$ values at which the eigenvalues are
assessed are determined in the absence of SOC.
In other words, the $\alpha_\text{K}$ values are largely insensitive to SOC.

To summarize, we do not include SOC when determining $\alpha_K$ but fully account 
the SOC when comparing defect levels to experiment.

The effect of SOC on the $\alpha_\text{K}$ values is clarified in the revised manuscript (p.~2).

\textit{5. Much of the argumentation rests on the claim that defect properties 
are not properly captured with the mixing parameter set to $\alpha_G$. 
And yet, at this mixing parameter in Fig.~1, an extrapolation of 
defect levels is used and not actual single-particle levels. This 
seems needless, why can these values not be calculated?}

Following the referee's suggestion, the calculated single-particle levels at $\alpha_G$
are given in Fig.~1. In cases where a localized defect level is no longer stabilized 
within the band-gap at such a large $\alpha$ (e.g., for Co$_\text{S}$), 
we use the linear extrapolation to indicate where the defect level would be if it
was localized.

\textit{6. The authors attempt to show that the same behavior occurs for HSE 
in addition to PBE0, and they state in the conclusions that ``using a 
fixed amount of Fock exchange in hybrid functionals could lead to [a] 
qualitatively incorrect description of defects in 2D materials''. 
However, I do not find convincing evidence that HSE suffers from the 
problems described in Figs.~1 and 2, and there are a number of papers 
claiming that tuned HSE calculations can accurately predict defect 
properties and the band gap in 2D materials (e.g., Ref.~8 in this 
work). Do $\alpha_G$ and $\alpha_K$ differ considerably for defects in \ce{WS2}? 
}

Indeed, we find that the $\alpha_\text{K}$ values are considerably smaller than 
the $\alpha_\text{G}$ even with HSE.
The large discrepancy is manifested by the new Fig.~S4 of the revised SM for the two defects 
(V$_\text{S}$ and Co$_\text{S}$) in \ce{WS2}.
The conclusion is therefore unaffected by range separation.

About Ref.~8, we do not find strong evidence supporting the authors' claim that 
the Koopmans' condition is satisfied with HSE($\alpha_\text{G}=0.4$).
The plot of KS eigenvalues for the boron dangling bond (Fig.~3) actually suggests a
strong concavity as indicated by the sizable difference between the single-particle 
eigenvalues (over 3~eV for $+1/0$ and 1.5~eV for $0/-1$).
These large differences are unlikely to be compensated by structural relaxations.
We would also like to note that Ref.~8 takes the reference band gap of 6.4~eV for 
\textit{h}-BN whereas some more recent experiments point to the fundamental gap of 6.8~eV
(2D Mater 8, 044001) and $G_0W_0$ gives 7~eV (PRMater 2, 124002).
If these larger band gap values are used to determine $\alpha_\text{G}$,
the gap between $\alpha_\text{K}$ and $\alpha_\text{G}$ would further widen.

While it is beyond the scope of our work to assess the 
scientific accuracy of Ref.~8 and other papers using 
HSE or other hybrid functionals for defects in 2D materials,
our work here underlines the importance of treating localized defects and band-edge positions
separately with appropriate $\alpha$ values.
This issue has long been overlooked, and it becomes particularly relevant for applications 
in emerging quantum information science.

\textit{7. I do not understand why $G_0W_0$, $\alpha_K$, and $\alpha_G$ have the same 
band offsets in Fig.~2. Clearly $\alpha_K$ and $\alpha_G$ have different 
band gaps, and cannot have the same band edges. Also, Fig.~S4 implies 
that $G_0W_0$ and tuned PBE0 can have a band offset. Furthermore, how is 
the experimental gap aligned to the calculations?}

As PBE0($\alpha_\text{K}$) underestimates the band gap, the band-edge positions for $\alpha_\text{K}$ 
are the ones obtained with $\alpha_\text{G}$
in Fig.~2 so that the defect levels (which are calculated at $\alpha_\text{K}$) can be compared readily to 
the $G_0W_0$ and PBE0($\alpha_G$) results.
The band offset between $G_0W_0$ and PBE0($\alpha_G$) (0.1~eV) is visible in Fig.~2(a) and in Fig.~S5 (previously S4
in the original SM).

Since the experimental band-edge positions are missing, we choose to align the defect levels 
with respect to the VBM when comparing to experiment [see Fig.~2(b)].
For simplicity, the experimental gap is set to the $G_0W_0$+SOC value (2.58~eV).
Such a choice does not affect the comparison of defect levels when the VBM is taken as the reference.
We have revised the caption of Fig.~2 to clarify this concern.

\subsection*{Response to Referee 2}
The referee provides a thorough summary of our work followed by a critique of the generality of the results.
We are thankful for the comments.
In essence, the referee considers our work  ``definitely contains results that should be published as a Letter'' 
but suggests that the presentation of the manuscript needs to be improved.

\textit{The paper is about the common practice of using the PBE0 form of hybrid functionals, 
adjusting the mixing parameter $\alpha$ to fit either an experimental value of the band gap or 
a value calculated by higher-level theory like $GW$, and then using the functional to calculate defect energy levels. 
For defects in bulk materials, the process works well because the “generalized Koopmans’ condition” (GKC) is satisfied. 
The authors’ ``first key result'' is to demonstrate that the same procedure does not work for defects in 2D materials simply 
because the GKC is not satisfied. Figure~1 shows clearly that fitting the band gap of ML \ce{WS2} 
requires $\alpha=\alpha_\text{G}=0.22$ 
while satisfying the GKC requires $\alpha=\alpha_\text{K}$ that is different for different defects and even different 
for two charge states of the same defect. The defect energy levels obtained with the two $\alpha$'s for several defects, 
shown in Figure~2, can be quite large, with the worst case being Co$_\text{S}^-$.}

We thank the referee for the accurate summary of our first key result showing that $\alpha_\text{K}$ 
is significantly different from $\alpha_\text{G}$. 
This is clearly in contrast to defects in bulk materials.

\textit{The second part of the paper is to demonstrate that the energy levels calculated using $\alpha_K$, 
i.e., the $\alpha$ that satisfies the GKC, are accurate, when compared with energy levels calculated using $G_0W_0$ 
and with experimental values (it would have been nice if they said so at the beginning of that part of the paper!). 
Once they have a PBE0-calculated $\alpha_K$, they do a $G_0W_0$($\alpha_K$) calculation for the Co$_\text{S}^-$ energy level 
and find that it agrees very well with the level obtained by the PBE0($\alpha_K$) calculation, 
whereas the PBE0($\alpha_G$) level is off by 1.3 eV. 
They find similar results for the Co$_S^0$ level 
(but they do not tell us if they used two different $\alpha_K$ as determined in Fig.~1). 
They conclude that the PBE0($\alpha_K$) defect levels are very accurate 
and propose that the energy bands should be calculated using $\alpha_G$ but defect levels should be calculated using $\alpha_K$. 
The alignment of band edges and defect level can be done by referencing both to the vacuum level. 
They further illustrate the validity of the proposed scheme by demonstrating that their PBE0($\alpha_K$) levels of Co$_S^-$ 
and Co$_S^0$ agree with experimental data. 
The rest of the paper is an attempt to say that their scheme has a physical justification.}

We did not show the Co$_\text{S}^0$ level in Fig.~2(a) as the level would coincide with Co$_\text{S}^-$ 
if calculated using $\alpha_K$.
Instead we show the C$_\text{S}^0$ level calculated with $\alpha_\text{K}=0.075$ as determined in Fig.~1.
We would also like to point out that V$_\text{S}^0$ and C$_\text{S}^-$ 
(rather than Co$_\text{S}^-$ and Co$_\text{S}^0$) are used to compare with 
experimental data to show the validity of the PBE0($\alpha_\text{K}$) method.

Following the referee's suggestion, we now make it clear at the beginning of that part of the paper 
that the PBE0($\alpha_\text{K}$) gives accurate defect levels (see p.~3). 
We further move the computational details to the SM to make the discussion more readable.

\textit{Nice work, but the authors need to explore the consequences for studies of defect physics. 
From a computational perspective, calculations of the total energy of the perfect crystal using $\alpha_G$ 
and of the defected crystal using $\alpha_K$ appear to be valid applications of density functional theory. 
But, once you align the defect levels with the energy bands relative to the vacuum level, 
the system is no longer described by a unique electron density corresponding to the “external potential”, 
which a requirement at the core of DFT. 
The energy bands and Bloch functions and the defect levels and wave functions are determined by different Hamiltonians, 
containing different potentials that are related to different charge densities by Poisson’s equation 
(there may be different Hamiltonians for different energy levels of one defect).}

Our present work focuses on the accurate description of defect energy levels in 2D materials,
which as we have demonstrated is attainable through the proposed scheme combining $\alpha_\text{G}$ and $\alpha_\text{K}$
for treating the band-edge positions and defect levels separately.
The need for two distinct $\alpha$ values (or two different Hamiltonians) indeed poses some difficulty
in applying the current scheme directly on top of the (g)KS-DFT in a broader sense.
The referee points to a series of questions concerning the general applicability of the present scheme 
which we try to respond as follows.

\textit{a) Is there a rigorous, DFT-sanctioned definition of defect formation energies? 
Formation energies are super important for many different purposes.}

Defect formation energy is generally expressed as 
$E_\text{tot}(D_q) - E_\text{tot}(\text{bulk}) + \Sigma_i n_i\mu_i + q (\epsilon_F + \epsilon_v$),
where $E_\text{tot}(D_q)$ is the total energy of defect $D$ in charge state $q$,
$E_\text{tot}(\text{bulk})$ is the total energy of the pristine host,
$n_i\mu_i$ are the chemical potential of the defect species $i$,
and $\epsilon_F$ is the Fermi energy typically referred to the VBM $\epsilon_v$.
We first consider the neutral charge state for which the formation energy reduces 
to the total-energy difference irrespective of electron chemical potential and hence band-edge positions.
In this case, the choice of $\alpha_K$ is easily justified to calculate the defect formation energy.
For charge defects where the band-edge positions become important,
one can still envisage total-energy calculations with $\alpha_K$ followed by 
corrections on the band-edge positions using the $\alpha_G$ reference.
This is essentially akin to the band-edge corrected DFT formation energy 
which is often very close to the formation energy obtained with full hybrid-functional calculations
for defects in bulk materials (see for example Fig.~6 of Phys. Status Solidi B 248, 775).
While plausible, the efficacy of such a scheme needs to be validated but this is beyond of the scope of 
the present work.

\textit{b) Fermi's golden rule requires that electronic transitions must be between eigenstates of a single Hamiltonian. 
How would one calculate optical absorption by a defect, namely transitions between the defect level and the energy bands? 
Such calculations are important for defects that can be used for quantum information.}

Naively one may attempt to adjust the band-edge eigenstates and the defect eigenstates in accord to 
the $\alpha_\text{G}$ and the $\alpha_\text{K}$ calculations, respectively.
The eigenfunctions can retain their descriptions given by $\alpha_\text{K}$.
This modified single-particle starting point can be used for subsequent calculations for optical absorption,
either in independent-particle approximation and random-phase approximation, or in Bethe-Salpeter equation and time-dependent DFT.
In fact, the scissor operation has been extensively applied to the calculation of optical absorption,
and we expect it to be useful for studying optical properties of defects in 2D materials.
This is a subject of future work.

\textit{(c) How do you calculate the dielectric response of a crystal containing defects 
(this question is related to the previous question as the imaginary part of the dielectric constant 
is proportional to the optical absorption coefficient). How do you calculate any linear response function in the
presence of defects.}

As the dielectric response is essentially optical, we refer to our response to the question (b).

\textit{(d) How do you calculate carrier recombination via defect levels that are crucial for defect-mediated 
device degradation as in LEDs? Again, Fermi's golden rule.}

The radiative recombination relies on the Fermi's golden rule (or more specifically the transition dipole moment), 
which we have discussed in (b). 
For the nonradiative recombination, our work does not address phonons and it is unclear about the implication of our
work on forces. 
We note that many phonon computations on defects are still done at the semilocal DFT level and 
mixed with the electronic structure obtained with hybrid functionals, which often 
give a highly accurate description of vibrational modes and electron-phonon coupling 
(see for example New J.\ Phys.\ 16, 073026; PRB 104, 045303). 

\textit{(e) How do you calculate phonons in the presence of defects?}

Please refer to our response to the question (d).

\textit{(f) How do you do DFT molecular dynamics in the presence of defects when defect level move
about the gap and appear or disappear?}

For MD simulations, we do not see immediately a viable solution to correctly position the defect levels and band edges
concurrently. But again, this is beyond what we are trying to address in the present work.

\textit{I am sure there are more questions one can ask. 
It seems to me that the paper misses the point that the energy level is only one number 
that you can get very accurately by introducing a new Hamiltonian for it. 
The accuracy resulting from employing the GKC is tantalizing, 
but I doubt you can go beyond that and do defect physics with two Hamiltonians. 
Nevertheless, the first part of the paper is important and the community needs to know that for 2D materials 
there is no unique $\alpha$ that gets a correct band gap and can also produce accurate energy levels. 
The result is really a major blow to hybrid functionals, but so be it. 
I urge the authors to figure out how to present these results to the community 
even if a resolution to the difficulty cannot be found. 
I would be a happy to review such a resubmission.}

We appreciate the stimulating questions brought up by the referee.
We emphasize that the present work focuses on defect energy levels.
Other properties (such as formation energy, optical absorption, and phonons)
that are equally important for defect physics in 2D materials, will most certainly be affected as well
and we hope that our results on the nonuniqueness of $\alpha$ will motivate answering these questions.

The failure of a singe-$\alpha$ hybrid-functional scheme for describing defects in 2D materials 
stems from the fact that the screening is static and position independent.
Our work therefore calls for the development of position-dependent hybrid functional.
On the other hand, we show that higher-level theories (such as $GW$, RPA, quantum Monte-Carlo, etc)  
are still invaluable for studying defects in 2D materials. 
But at the same time we are not aware of a single method that can compute all defect properties.
For example, forces are difficult to obtain from $GW$ and so are phonons.
We notice some recent development of Koopmans' compliant density functional for periodic systems
(Colonna arxiv:2202.08155). This could be a viable approach to studying defect physics in 2D materials.

In the revised manuscript, we have added a paragraph before conclusion 
indicating that we did not address all the problems related to defects in 2D materials 
and our work motivates further investigation of the implications for 
optical properties, phonons, carrier recombinations, etc.

\textit{I should also note that I did not find the last section of the paper very palatable. 
Talk about the dielectric constant of a supercell as opposed to a physical crystal leaves me cold. 
There is apparently quite a bit of literature on this and related concepts. 
I would leave that out of a revised version of this paper.}

The paragraph on the dielectric constant of the \ce{WS2} supercell has been removed in the revised version.
Figure~3(c) is removed accordingly.

\subsection*{Response to Referee 3}
We first thank the referee for the positive recommendation.

\textit{1) For the two sub-figures on the right of Figure~1, the HO energy is 
lower than the LU energy for the same defect with the same charge at $\alpha=0.05$. Is it wrong?}

For the same defect in the same charge state, the highest occupied (HO) energy should be indeed 
lower than the lowest unoccupied (LU) energy. This was correctly shown in Fig.~1 for the neutral Co$_\text{W}$
and the neutral Co$_\text{S}$. 

\textit{2) On Figure 3a, the authors mark the calculated defect level of $V_\text{S}$ as 
the red circle for bulk \ce{WS2}. Why does it have an optimal $\alpha$ that 
is so much different from other defects in bulk? Is it because this 
defect has a different screening effect? Did I miss anything that’s 
already mentioned?}

When reviewing the $V_\text{S}$ results in bulk \ce{WS2}, we spot an error in the finite-size corrections.
With the corrected eigenvalues the $\alpha_\text{K}$ value is 0.12, which is more consistent with the 
value for the monolayer \ce{WS2}.
We thank the referee for pointing this out.
Figure~3(a) has been updated in the revised manuscript.

\textit{3) The input files or at least the optimized structures should be 
provided in the SM.}

All input and output files are now made available in an online repository
as indicated in the ``Data availability'' section of the revised SM.

\end{document}
