\documentclass[amsmath,amssymb,aps,prl,reprint,floatfix,superscriptaddress]{revtex4-2}

\usepackage{graphicx}% include figure files
\usepackage{dcolumn}% align table columns on decimal point
\usepackage{bm}% bold math
\usepackage[version=4]{mhchem}
\usepackage[breaklinks,colorlinks,linkcolor={blue}, %
            citecolor={blue},urlcolor={blue}]{hyperref}
\usepackage[T1]{fontenc}

\makeatletter
\renewcommand\thetable{S\@arabic\c@table}
\renewcommand\thefigure{S\@arabic\c@figure}
\makeatother

\begin{document}

\title{\textsc{Supplemental Material}\\
Non-unique fraction of Fock exchange for defects in two-dimensional materials}
\author{Wei Chen}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN), Universit\'{e} catholique de Louvain, Louvain-la-Neuve 1348, Belgium}
\author{Sin\'ead M. Griffin}
\affiliation{Molecular Foundry Division, Lawrence Berkeley National Laboratory, Berkeley, California 94720, USA}
\affiliation{Materials Sciences Division, Lawrence Berkeley National Laboratory, Berkeley, California 94720, USA}
\author{Gian-Marco Rignanese}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN), Universit\'{e} catholique de Louvain, Louvain-la-Neuve 1348, Belgium}
\author{Geoffroy Hautier}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN), Universit\'{e} catholique de Louvain, Louvain-la-Neuve 1348, Belgium}
\affiliation{Thayer School of Engineering, Dartmouth College, Hanover, New Hampshire 03755, USA}
\date{\today}

\maketitle

\section{\label{sec:comp}Computational details}
We employ two orthorhombic supercells to represent the monolayer (ML) \ce{WS2}. 
The majority of hybrid functionals are carried out with the 90-atom orthorhombic supercell by transforming the 
in-plane lattice vectors of the primitive cell through the matrix 
$\big(\begin{smallmatrix}
5 & 5 \\ -3 & 3 
\end{smallmatrix}\big)$.
The out-of-plane lattice parameter $L_z=24$ \AA.
A smaller 36-atom orthorhombic supercell is obtained via the transformation matrix 
$\big(\begin{smallmatrix}
3 & 3 \\ -2 & 2 
\end{smallmatrix}\big)$ with $L_z=16$ \AA.
The smaller supercell is used in $G_0W_0$ calculations and for analyzing the finite-size effect.
The experimental in-plane lattice constant of the bulk 2\textit{H}-\ce{WS2} ($a=3.15$ \AA~\cite{Schutte1987}) is used 
without further relaxation for the ML \ce{WS2}.

The kinetic energy cutoff is 340~eV for the projector-augmented wave (PAW) calculations.
For the carbon substitutional defect (C$_\text{S}$), the cutoff is increased to 400~eV.
We use a $k$-point mesh density equivalent to $12\times12\times1$ for the primitive cell of ML \ce{WS2} 
(e.g., $4\times4\times1$ for the 36-atom supercell and $3\times3\times1$ for the 90-atom supercell).
The evaluation of Fock exchange is sped up with the adaptively compressed exchange operator formulation~\cite{Lin2016}
with an energy cutoff of 270~eV (320~eV for C$_\text{S}$).
The atomic coordinates are relaxed until the residual forces are below 0.02~eV/\AA.

The mean-field starting point for the one-shot $G_0W_0$ calculations are carried out 
using the optimized norm-conserving pseudopotentials~\cite{Hamann2013} 
made available via the \textsc{PseudoDojo} project~\cite{vanSetten2018}.
We use the $4\times4\times1$ $\Gamma$-centered $k$-point mesh for the 36-atom orthorhombic supercell.
The dielectric matrix is evaluated with up to 5000 bands and a planewave energy cutoff of 10~Ry.
Frequency dependence of dielectric function is taken into account by the Godby-Needs plasmon-pole model~\cite{Godby1989}.
The truncated Coulomb interaction is applied along the out-of-plane direction~\cite{IsmailBeigi2006}.
The slow convergence with respect to $q$ points is addressed by the subsampling technique~\cite{Jornada2017},
which includes 3 additional $q$ points for $q\rightarrow0$.
Spin-orbit coupling is not included in $G_0W_0$ calculations.

{We find the QP energies can be reasonably described with a smaller
orthorhombic supercell of 36 atoms owing to the strong localization of the defect.
For results depicted in Fig.~2(a) of the main text,
we use the PBE equilibrium configuration of the neutral defect
and refrain from further relaxations for the negatively charged defect
in order to exclude the structural effect.}

\section{\label{sec:correction}Finite-size corrections}
%\subsection{Total energy}
The total energies of charged point defects are corrected by the
\textit{a posteriori} correction scheme~\cite{Komsa2013,Komsa2014,FarzalipourTabriz2019}.
Since ionic contributions to the dielectric screening is negligible for \ce{WS2},
we use the high-frequency dielectric constant throughout.
Figure~\ref{fig:corr_energy} shows the formation energies of 
\textit{V}$_\text{S}$ and Co$_\text{S}$ in the ML \ce{WS2}.
Co$_\text{S}$ exhibits strongly localized defect wavefunctions whereas \textit{V}$_\text{S}$ is less localized.
Overall the corrected formation energies for the charged defects are converged already with the 36-atom supercell regardless of defect localization.

\begin{figure}
\includegraphics{figs/formation.pdf}
\caption{\label{fig:corr_energy}Uncorrected and corrected formation energies for the 
\textit{V}$_\text{S}$ and Co$_\text{S}$ defects calculated with DFT-PBE. 
For the series of supercells with 36, 144, 324, and 576 atoms (denoted by circles) 
the supercell length $L_z=1.6(L_xL_y)^{1/2}$. 
For the 90-atom supercell (denoted by triangles) $L_z=1.5(L_xL_y)^{1/2}$.
For the charged defects, the uncorrected (corrected) formation energies are shown by the empty (filled) circles.} 
\end{figure}

%\subsection{Single-particle eigenvalues}
For charged defects in the ML \ce{WS2}, the electrostatic potential and eigenvalues are corrected concurrently using 
the scheme of Chagas da Silva \textit{et al.}~\cite{ChagasdaSilva2021}.
Figure~\ref{fig:corr_eigenvalue} shows the Kohn-Sham (KS) single-particle eigenvalues associated with 
\textit{V}$_\text{S}$ and Co$_\text{S}$.
The electrostatic potential furthest away from the defect is taken as the vacuum reference level.
While the single-particle eigenvalues are well defined for the neutral defects,
they are subject to strong finite size effect as the supercell size varies.
The potential correction scheme works well for the negatively charged Co$^{-}_\text{S}$ 
and the corrected eigenvalue is already converged within 0.1~eV with the smallest 36-atom supercell.
For \textit{V}$^-_\text{S}$ the slower convergence of the corrected eigenvalues can be attributed 
to the more delocalized nature of the defect wavefunctions.
Nonetheless, the eigenvalue of \textit{V}$^-_\text{S}$ is still reasonably converged within 0.2~eV with the 36-atom supercell.

For defects in the bulk \ce{WS2}, we apply the eigenvalue correction $-\frac{2}{q}E_\text{corr}$ 
where $E_\text{corr}$ is the total-energy correction~\cite{Chen2013}.

\begin{figure}
\includegraphics{figs/eigenvalue_vac.pdf}
\caption{\label{fig:corr_eigenvalue}Uncorrected and corrected KS single-particle eigenvalues
with respect to the vacuum level calculated with DFT-PBE.}
\end{figure}

\section{Role of long-range Fock exchange for 2D systems}
The range-separated HSE hybrid functional only retains the Fock exchange in the short range.
This is practical for 3D semiconductors as it accelerates the computation, 
but for 2D systems the short-range hybrid functional is insufficient to account for the band-gap opening at the reduced dimension.
While it is in principle possible to recover the band gap with an exceedingly large $\alpha$ in HSE,
the band structure is visibly distorted.
This is manifested, for instance, by the too large VBM bandwidth and the overestimated 
direct band gap at $\Gamma$ (cf.\ Fig.~\ref{fig:bs}).
On the other hand, the band structure of PBE0($\alpha_\text{G}$) is in much better agreement with the $G_0W_0$ one.
In addition, the large $\alpha$ places the HSE band edges about 0.3~eV too deep compared to the $G_0W_0$,
PBE0($\alpha_\text{G}$) again achieves an excellent agreement in terms of the absolution band-edge positions.

\begin{figure}
\includegraphics{figs/bs.pdf}
\caption{\label{fig:bs}Band structure of ML WS$_2$ (top panel) and absolute band-edge positions (bottom) 
obtained with the short-range hybrid functional (HSE) and the global hybrid functional retaining the long-range part of the Fock exchange (PBE0). 
In the band-structure plot both functionals use a mixing parameter $\alpha$ leading to the $G_0W_0$ band gap.  
The $G_0W_0$ band structure is shown in dashed lines. 
The energy is referred to the top of the VBM at $K$.
The band-edge positions are shown as a function of the mixing parameter $\alpha$.
The $\alpha$ value at which the $G_0W_0$ band gap is reproduced is highlighted.
The $G_0W_0$ band-edge positions are indicated by the dashed lines.
}
\end{figure}

\begin{figure}
\includegraphics{figs/be_ks_hse.pdf}
\caption{\label{fig:be_ks_hse}{Single-particle defect levels as a function of $\alpha$ with the range-separated 
HSE functional. The inverse screening length is set to 0.2 \AA$^{-1}$. For legends refer to Fig.~1 of the main text.}}
\end{figure}

\begin{figure}
\includegraphics{figs/cos_g0w0.pdf}
\caption{\label{fig:cos_g0w0}Single-particle defect level of Co$^-_\text{S}$ calculated with two families of hybrid functionals (PBE0
and HSE) and compared to $G_0W_0$ reference. The defect levels are referred to the VBM (left) and to the vacuum level (right).
For the hybrid functionals, the band edges are obtained with the optimal mixing parameter ($\alpha_\text{G}$) 
which reproduces the $G_0W_0$ band gap, 
whereas the defect levels are determined by the mixing parameter ($\alpha_\text{K}$) fulfilling Koopmans' condition.
Concretely, $\alpha_\text{G}$ is 0.22 (0.55) for PBE0 (HSE), and $\alpha_\text{K}$ is 0.07 (0.07) for PBE0 (HSE).
The inverse screening length is fixed at 0.2~\AA$^{-1}$ for HSE.}
\end{figure}

The too deep VBM has an implication for interpreting defect levels when referenced to the band edges.
As shown in Fig.~\ref{fig:cos_g0w0}, the HSE single-particle defect level of Co$^-_\text{S}$ is about 0.3~eV 
higher than those of PBE0 and $G_0W_0$ as a result of the misaligned VBM.
Nevertheless, HSE still leads to consistent descriptions of defect levels insofar as the vacuum level 
is used as the common reference.

Figure~\ref{fig:be_ks_hse} shows the single-particle defect levels as a function of $\alpha$ with the range-separated HSE functional.
We obtain $\alpha_\text{K}=0.21$ for V$_\text{S}^{0/-}$ and $\alpha_\text{K}=0.07$ for Co$_\text{S}^{0/-}$.
In accord with the PBE0 results, these values are considerably smaller than the $\alpha_\text{G}$ (0.55).
The sizable discrepancy between $\alpha_\text{K}$ and $\alpha_\text{G}$ is therefore universal for 2D materials 
regardless of range separation.

\section{Koopmans' condition and spin-orbit coupling}
{The values of $\alpha_\text{K}$ (i.e., the mixing parameter fulfilling the Koopmans' condition) 
have been determined without taking into account spin-orbit coupling (SOC) in the main text.
We show in Table~\ref{tab:soc} that Koopmans' condition is still largely fulfilled at these $\alpha_\text{K}$ values
if the SOC is taken into account.
In particular, the two single-particle eigenvalues agree within 0.1 eV for all the defects considered in this work.}

\begin{table}
\caption{\label{tab:soc}{Single-particle eigenvalues (in eV) of the pertinent defect states with respect to the vacuum level
including the SOC effect. The fraction of Fock exchange at which the eigenvalues are evaulated follows the $\alpha_\text{K}$ values
from the main text without taking into account SOC. 
For any given defect, the equilibrium structure of the charged defect does not incude the SOC effect 
and is used for the neutral one.}}
\begin{ruledtabular}
\begin{tabular}{ldddddd}
 &
\multicolumn{1}{c}{V$^{-1}_\text{S}$} &
\multicolumn{1}{c}{C$^{-1}_\text{S}$} &
\multicolumn{1}{c}{Co$^{-1}_\text{W}$} &
\multicolumn{1}{c}{Co$^{+1}_\text{W}$} &
\multicolumn{1}{c}{Co$^{-1}_\text{S}$} &
\multicolumn{1}{c}{Co$^{+1}_\text{S}$} \\[0.5ex]
\hline
\rule{0pt}{3ex}$\varepsilon(q)$ & -4.25 & -4.81 &  -4.69 & -5.23  & -4.17 & -5.23\\
$\varepsilon(0)$ & -4.18 & -4.88 &  -4.76 & -5.32  & -4.26 & -5.23\\
\end{tabular}
\end{ruledtabular}
\end{table}
 

\section{Koopmans' condition for monolayer boron nitride}
\begin{table}
\caption{\label{tab:hbn}Highest occupied eigenvalues of the negatively charged C$_\text{N}^{-}$ [$\varepsilon^\text{HO}(-)$]
and lowest unoccupied eigenvalues of the neutral C$_\text{N}^0$ [$\varepsilon^\text{LU}(0)$]
obtained with $\alpha_\text{G}^\text{ML}$ along with a vanishing $\alpha$ value and the standard $\alpha=0.25$.
The equilibrium structure of C$_\text{N}^{-}$ obtained with the PBE functional is used throughout.
The eigenvalues are referred to the vacuum level and are in eV.
The deviation from piecewise linearity is indicated by $\Delta$.
A positive (negative) $\Delta$ value corresponds to convexity (concavity).}
\begin{ruledtabular}
\begin{tabular}{lddd}
             & \multicolumn{1}{c}{$\alpha=0$} & 
               \multicolumn{1}{c}{$\alpha=0.25$} &
               \multicolumn{1}{c}{ $\alpha_\text{G}^\text{ML}=0.35$} \\[0.5ex]
\hline
\rule{0pt}{3ex}$\varepsilon^\text{HO}(-)$ & -3.50 & -4.34  &  -4.70 \\   
$\varepsilon^\text{LU}(0)$ & -5.11 & -4.42  &  -4.08 \\
$\Delta$                   &  1.61 &  0.08  &  -0.62 \\
\end{tabular}
\end{ruledtabular}
\end{table}

We extend the analysis to the hexagonal boron nitride (\textit{h}-BN), a simple $sp$ semiconductor.
We use the C$_\text{N}$ substitutional defect to probe to what extent the Koopmans' condition is fulfilled at 
given $\alpha$ values.
The band gap of the ML \textit{h}-BN, which corresponds to the indirect $K-\Gamma$ transition,
is 7.0~eV according to the $G_0W_0$ calculations of Ref.~\cite{Smart2018}.
To reproduce this $G_0W_0$ band gap, the mixing parameter $\alpha_\text{G}^\text{ML}$ of 0.35 is needed for
the PBE0 hybrid functional.
In comparison, the standard PBE0($\alpha=0.25$) describes the band gap of the bulk \textit{h}-BN reasonably well.

The defect calculations are carried out with a 72-atom orthorhombic supercell with $L_z=15$~\AA\ using 
an energy cutoff of 400~eV and a $\Gamma$-centered $4\times4\times4$ $\mathbf{k}$-point mesh.
Table~\ref{tab:hbn} shows the highest occupied and the lowest unoccupied eigenvalues pertinent to the
determination of Koopmans' condition at three representative $\alpha$ values, namely 0, 0.25, and $\alpha_\text{G}^\text{ML}$.
As expected, the Koopmans' condition is best satisfied at $\alpha=0.25$, the value closely reproducing the band gap 
of the bulk \textit{h}-BN.
The other two values lead to either strong convexity or strong concavity.
The Koopmans' $\alpha_\text{K}$ value is estimated to be 0.26, significantly lower than the $\alpha_\text{G}^\text{ML}$. 
This shows that the non-uniqueness of $\alpha$ is a general attribute applicable to a wide class of 2D materials.

\section{Data availability}
{The input and output files for the defect calculations presented in this work are available at 
\url{https://doi.org/10.5281/zenodo.6916999}.}


\bibliography{main}
\end{document}
