#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 4.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Set2')

df = pd.read_csv('formation_no_90.dat', delim_whitespace=True, comment='#')
fig = plt.figure()
gs = fig.add_gridspec(1,2, width_ratios=[1,1], left=0.12, right=0.96, top=0.92, bottom=0.15, wspace=0.2)

gs0 = gs[0].subgridspec(2,1, hspace=0.1)
gs1 = gs[1].subgridspec(2,1, hspace=0.1)

ax00 = fig.add_subplot(gs0[0])
ax01 = fig.add_subplot(gs0[1])

#inverse_omega = 1.0/(df['x']*df['y']*df['z'])**(1./3)

df0 = df
inverse_area = 1.0/(df0['x']*df0['y'])**(1./2)

ax01.scatter(inverse_area, df0['V_S(0)'],c='C0')
ax01.plot(inverse_area, df0['V_S(0)'],c='C0',ls='--',lw=0.6)
ax00.scatter(inverse_area, df0['V_S(-1)'],ec='C2',fc='none')
ax00.plot(inverse_area, df0['V_S(-1)'],c='C2',ls='--',lw=0.6)
ax00.scatter(inverse_area, df0['V_S(-1)_corr'],c='C2')
ax00.plot(inverse_area, df0['V_S(-1)_corr'],c='C2',ls='--',lw=0.6)

ax10 = fig.add_subplot(gs1[0])
ax11 = fig.add_subplot(gs1[1])

ax11.scatter(inverse_area, df0['Co_S(0)'],c='C0')
ax11.plot(inverse_area, df0['Co_S(0)'],c='C0',ls='--',lw=0.6)
ax10.scatter(inverse_area, df0['Co_S(-1)'],ec='C2',fc='none')
ax10.plot(inverse_area, df0['Co_S(-1)'],c='C2',ls='--',lw=0.6)
ax10.scatter(inverse_area, df0['Co_S(-1)_corr'],c='C2')
ax10.plot(inverse_area, df0['Co_S(-1)_corr'],c='C2',ls='--',lw=0.6)

ax01.tick_params(axis=u'x', which=u'both',top=False)
ax11.tick_params(axis=u'x', which=u'both',top=False)
ax00.tick_params(axis=u'x', which=u'both',bottom=False)
ax10.tick_params(axis=u'x', which=u'both',bottom=False)

for ax in [ax00,ax01,ax10,ax11]:
    ax.set_xlim([0,0.10])

ax00.set_ylim([2.65,3.3])
ax01.set_ylim([1.03,1.68])
ax10.set_ylim([3.85,4.6])
ax11.set_ylim([1.8+0.8-0.1,2.35+0.8+0.1])

ax00.set_ylabel('Formation energy (eV)')
ax00.yaxis.set_label_coords(-0.2,-0.2)
ax01.set_xlabel(r'$(L_{x}L_{y})^{-{1}/{2}}$ ($\AA^{-1}$)')
ax11.set_xlabel(r'$(L_{x}L_{y})^{-{1}/{2}}$ ($\AA^{-1}$)')

ax00.spines['bottom'].set_visible(False)
ax01.spines['top'].set_visible(False)
ax10.spines['bottom'].set_visible(False)
ax11.spines['top'].set_visible(False)
#ax01.xaxis.tick_top()
#ax00.xaxis.tick_bottom()
#ax11.xaxis.tick_top()
#ax10.xaxis.tick_bottom()
ax00.tick_params(top=False,bottom=False,labelbottom=False)
ax10.tick_params(top=False,bottom=False,labelbottom=False)

d=.015
for ax in [ax00,ax10]:
    kwargs = dict(transform=ax.transAxes, color='k', clip_on=False, lw=0.8)
    ax.plot((-d, +d), (-d, +d), **kwargs) 
    ax.plot((1 - d, 1 + d), (-d, +d), **kwargs) 

for ax in [ax01,ax11]:
    kwargs = dict(transform=ax.transAxes, color='k', clip_on=False, lw=0.8)
    ax.plot((-d, +d), (1-d, 1+d), **kwargs) 
    ax.plot((1 - d, 1 + d), (1-d, 1+d), **kwargs) 

ax01.text(0.1,0.8,r'V$_S^0$',transform=ax01.transAxes)
ax00.text(0.1,0.8,r'V$_S^{-1}$',transform=ax00.transAxes)
ax11.text(0.1,0.8,r'Co$_S^0$',transform=ax11.transAxes)
ax10.text(0.1,0.8,r'Co$_S^{-1}$',transform=ax10.transAxes)

ax00t = ax00.secondary_xaxis('top')
ax10t = ax10.secondary_xaxis('top')

ticks = 1.0/(df['x']*df['y'])**(1./2)
label_top = ['36','144','324']
ax00t.set_xticks(ticks.values)
ax10t.set_xticks(ticks.values)
ax00t.set_xticklabels(label_top)
ax10t.set_xticklabels(label_top)

plt.savefig('formation_no_90.pdf')

plt.show()
