#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.65
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'hatch.linewidth': 0.5,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'xtick.bottom': False,
          'ytick.right': False,
          'axes.spines.top': False,
          'axes.spines.bottom': False,
          'axes.spines.right': False,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette("Paired")
cb = 'C0'
cd = 'C1'
cd_rel = 'C0'
fig = plt.figure()
gs = fig.add_gridspec(1,2,left=0.12, width_ratios=[1.5,1],right=0.88, top=0.96, bottom=0.10, wspace=0.2)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])

eg = 2.9
vbm_vac_hse_55 = -6.52
vbm_vac_pbe0_22 = -6.23
vbm_vac_g0w0 = -6.18
alpha = 0.4
xx = np.linspace(0,6,1000)
w = 0.5; h = 0.01
xmin = 0.5; xmax = 3-xmin
cm_hse = cm.Greens
cm_pbe0 = cm.Blues
cm_be = cm.Blues
ymin = -6.5; ymax = -3.0

df = pd.read_csv('cos_g0w0.dat', delim_whitespace=True, index_col=0)

def plot_vbm(series,vbm_vac_ref,labels,ax):
    dfa = df.copy()
    for ind, i in enumerate(series):
        dfa.loc['VBM',i] += vbm_vac_ref[ind] - dfa.loc['VBM_vac',i]
    for i in series:
        dfa[i] -= dfa.loc['VBM',i]

    ax.axhline(0, color=cb)
    ax.fill_between(xx, 0, -2, color=cb, alpha=alpha)
    ax.axhline(eg, color=cb)
    ax.fill_between(xx, eg, eg+2, color=cb, alpha=alpha)
    #k = 8
    #for i in range(k):
    #    ax.fill_between(xx,0-i*0.1,0-(i+1)*0.1, lw=0., color=cb, alpha=alpha*(1-i/k))
    #    ax.fill_between(xx,eg+i*0.1, eg+(i+1)*0.1, lw=0, color=cb, alpha=alpha*(1-i/k))

    for j in range(1,6):
        for orb in ['d1','d2','d3']:
            rect = patches.Rectangle((j-w/2,dfa.loc[orb,series[j-1]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
            ax.add_patch(rect)
        #ax.text(j,2.2, r'{0}'.format(labels[j-1]), ha='center')

    ax.set_xlim([xmin, xmax])
    ax.set_ylim([-0.5,3.5])

    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    for orb in ['d1','d3']:
        for j in range(1,5):
            ax.annotate('', xy=(j+w/2,dfa.loc[orb, series[j-1]]),xytext=(j+1-w/2,dfa.loc[orb, series[j]]), xycoords='data', arrowprops = props)

    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{VBM}$ (eV)')

    yl = 0.; yh=eg
    ax.imshow([[1.,0.],[1.,0]], cmap = cm_hse, extent = [0, 2.5, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .6, aspect = "auto")
    ax.imshow([[0.,1.],[0.,1]], cmap = cm_pbe0, extent = [3.5, 6, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .6, aspect = "auto")

    return dfa

def plot_vac(series,vbm_vac_ref,labels,ax,corr=0,relax=0):
    dfb = df.copy()
    for ind, i in enumerate(series):
        dfb.loc['VBM',i] -= dfb.loc['VBM_vac',i] - vbm_vac_ref[ind]
        for row in ['d1','d2','d3','VBM']:
            dfb.loc[row,i] -= dfb.loc['VBM',i] - vbm_vac_ref[ind]
    
    vb_vac = np.piecewise(xx, [(xx <= 1.5), (xx > 1.5)], [dfb.loc['VBM','g0w0'], dfb.loc['VBM',series[-1]]])
    ax.plot(xx, vb_vac, color=cb)
    ax.fill_between(xx, vb_vac, vb_vac-2, lw=0, color=cb, alpha=0.1)
    ax.fill_between(xx, vb_vac, vb_vac-2, lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)

    cb_vac = np.piecewise(xx, [(xx <= 1.5), (xx > 1.5)], [dfb.loc['VBM','g0w0']+eg, dfb.loc['VBM',series[-1]]+eg])
    ax.plot(xx, cb_vac, color=cb)
    ax.fill_between(xx, cb_vac, cb_vac+2, lw=0, color=cb, alpha=0.1)
    ax.fill_between(xx, cb_vac, cb_vac+2, lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)
    
    for j in range(len(series)):
        for orb in ['d1','d2','d3']:
            rect = patches.Rectangle((j+1-w/2,dfb.loc[orb,series[j]]+corr),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
            ax.add_patch(rect)
            # band label
            #ax.text(j+1+w/2*1.2, dfb.loc[orb,series[j]]+corr, '{0}'.format(dfb.loc[orb, 'band']), fontsize=5)

        if abs(relax) > 1E-2:
            orb = 'd3'
            rect = patches.Rectangle((j+1-w/2,dfb.loc[orb,series[j]]+corr+relax),w,h+0.01, ls='--', linewidth=0.5, edgecolor=cd_rel, facecolor=cd_rel, alpha=0.8)
            ax.add_patch(rect)
            props = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
            arrow_params = dict(length_includes_head=False, lw=0.5, ls='-', head_width=0.03, head_length=0.04, shape='full', overhang=0.5, color='0.4')
            ax.arrow(j+1,dfb.loc[orb,series[j]]+corr, 0, relax, **arrow_params)
        ax.text(j+1,-5.6, r'{0}'.format(labels[j]), ha='center')

    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])

    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    for orb in ['d1','d3']:
        for j in range(1,2):
            ax.annotate('', xy=(j+w/2,dfb.loc[orb, series[j-1]]+corr),xytext=(j+1-w/2,dfb.loc[orb, series[j]]+corr), xycoords='data', arrowprops = props)
            if orb == 'd3':
                ax.annotate('', xy=(j+1+w/2,dfb.loc[orb, series[j]]+corr+relax),xytext=(j+1+w/2+0.3,ctl + vbm_22), xycoords='data', arrowprops = props)
    yl = dfb.loc['VBM','g0w0']; yh = dfb.loc['VBM','g0w0']+eg
    ax.imshow([[0.5,0],[1.,0]], cmap = cm_pbe0, extent = [0, 1.5, yl, yh], interpolation = "bilinear", vmin=0, vmax=5.5, alpha = .6, aspect = "auto")

    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{vac}$ (eV)')

    ax.text(1, dfb.loc['d3','g0w0']+corr-0.2, r'$d_{xy}+d_{x^2-y^2}$',ha='center',fontsize=6)
    ax.text(1, dfb.loc['d1','g0w0']+corr-0.2, r'$d_{z^2}$',ha='center',fontsize=6)
    props = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
    ax.annotate("",xy=(2.5,dfb.loc['VBM','pbe0']), xycoords=ax.transData, xytext=(2.5,dfb.loc['VBM','pbe0']+eg),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.2", lw=0.5, shrinkA=1, shrinkB=1, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
    ax.text(2.5+0.15,dfb.loc['VBM','pbe0']+eg/2,r'$\alpha=0.22$',props,va='center',rotation=90, fontsize=6)
    ax.text(0.5,-0.09,r'Co$_S^-$', transform=ax.transAxes)
    return dfb

hatch = '////'
series = ['g0w0','pbe0']
vac_ref = [vbm_vac_g0w0,vbm_vac_pbe0_22]
labels = [r'G$_0$W$_0$', r'PBE0 ($\alpha^{kopt}$)']

dfe = pd.read_csv('cos_eform.dat', delim_whitespace=True)
vbm_5 = -5.78
vbm_22 = -6.23
yy = np.linspace(vbm_22-vbm_22,vbm_22-vbm_22+eg,10)
ax1.spines['bottom'].set_visible(True)
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(True)
ax1.spines['left'].set_visible(False)
ax1.get_xaxis().set_ticklabels([])
ax1.set_ylim([ymin-vbm_22,ymax-vbm_22])
ax1.set_xlim([2.0,2.8])

ax1.plot(-yy+dfe.iloc[1]['eform']+vbm_5-vbm_22,yy, color='C0', lw=1)
ax1.plot(dfe.iloc[0]['eform']+0*yy, yy, color='C0', lw=1)
ax1.invert_xaxis()

ctl = dfe.iloc[1]['eform']+vbm_5-vbm_22-dfe.iloc[0]['eform']
ax1.scatter(dfe.iloc[0]['eform'],ctl,color='C1', zorder=10)

xx2 = np.linspace(0,5,10)
ax1.fill_between(xx2,0,-1,lw=1., fc=cb, ec=cb, alpha=0.1)
ax1.fill_between(xx2,0,-1,lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)
ax1.fill_between(xx2,eg,eg+1,lw=1., fc=cb, ec=cb, alpha=0.1)
ax1.fill_between(xx2,eg,eg+1,lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)
ax1.plot(xx2,0*xx2,color=cb)
ax1.plot(xx2,eg+0*xx2,color=cb)
ax1.set_xlabel(r'$E_{form}$')
ax1.yaxis.set_label_position('right')
ax1.yaxis.tick_right()
ax1.set_ylabel(r'$\mu$ (eV)')
ax1.text(2.6,1.3,r'$q=0$',rotation='90')
ax1.text(2.5,2.2,r'$q=-1$',rotation='25')
ax1.annotate("",xy=(dfe.iloc[0]['eform'],ctl),xytext=(dfe.iloc[0]['eform']+0.3,ctl), xycoords=ax1.transData, textcoords=ax1.transData,arrowprops=dict(arrowstyle="-", color="0.2", lw=0.5, shrinkA=0, shrinkB=0, ls='dashed', patchA=None, patchB=None))
ax1.text(2.5,0.62,r'PBE0 ($\alpha^{kopt}$)')

dfb = plot_vac(series, vac_ref, labels, ax0, corr=0.372, relax=0.1)

plt.savefig('cos_g0w0_pbe0.png', dpi=600)
plt.savefig('cos_g0w0_pbe0.pdf', dpi=600)
