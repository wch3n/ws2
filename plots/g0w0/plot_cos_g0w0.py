#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.6
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'xtick.bottom': False,
          'ytick.right': False,
          'axes.spines.top': False,
          'axes.spines.bottom': False,
          'axes.spines.right': False,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette("Paired")
cb = 'C0'
cd = 'C1'
fig = plt.figure()
gs = fig.add_gridspec(1,2, left=0.12, right=0.98, top=0.96, bottom=0.05, wspace=0.4, hspace=0.4)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])

eg = 2.9
vbm_vac_hse_55 = -6.52
vbm_vac_pbe0_22 = -6.23
vbm_vac_g0w0 = -6.18
alpha = 0.2
xx = np.linspace(0,6,1000)
w = 0.5; h = 0.03
xmin = 0.5; xmax = 4-xmin
cm1 = cm.Blues
cm2 = cm.Blues

df = pd.read_csv('cos_g0w0.dat', delim_whitespace=True, index_col=0)

def plot_vbm(series,vbm_vac_ref,labels,ax, corr=0):
    dfa = df.copy()
    for ind, i in enumerate(series):
        dfa.loc['VBM',i] += vbm_vac_ref[ind] - dfa.loc['VBM_vac',i]
    for i in series:
        dfa[i] -= dfa.loc['VBM',i]

    ax.axhline(0, color=cb)
    ax.fill_between(xx, 0, -2, color=cb, alpha=alpha)
    ax.axhline(eg, color=cb)
    ax.fill_between(xx, eg, eg+2, color=cb, alpha=alpha)
    #k = 8
    #for i in range(k):
    #    ax.fill_between(xx,0-i*0.1,0-(i+1)*0.1, lw=0., color=cb, alpha=alpha*(1-i/k))
    #    ax.fill_between(xx,eg+i*0.1, eg+(i+1)*0.1, lw=0, color=cb, alpha=alpha*(1-i/k))

    for j in range(1,4):
        for orb in ['d1']:
            rect = patches.Rectangle((j-w/2,dfa.loc[orb,series[j-1]]+corr),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
            ax.add_patch(rect)
        ax.text(j,1.8, r'{0}'.format(labels[j-1]), ha='center', fontsize=5.5)

    ax.set_xlim([xmin, xmax])
    ax.set_ylim([-0.5,3.5])

    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    for orb in ['d1']:
        for j in range(1,3):
            ax.annotate('', xy=(j+w/2,dfa.loc[orb, series[j-1]]+corr),xytext=(j+1-w/2,dfa.loc[orb, series[j]]+corr), xycoords='data', arrowprops = props)

    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{VBM}$ (eV)')

    yl = 0.; yh=eg
    ax.imshow([[1.,0.],[1.,0]], cmap = cm1, extent = [0, 3.5, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .2, aspect = "auto")

    return dfa

def plot_vac(series,vbm_vac_ref,labels,ax, corr=0):
    dfb = df.copy()
    for ind, i in enumerate(series):
        dfb.loc['VBM',i] -= dfb.loc['VBM_vac',i] - vbm_vac_ref[ind]
        for row in ['d1','VBM']:
            dfb.loc[row,i] -= dfb.loc['VBM',i] - vbm_vac_ref[ind]
    print(dfb.loc['VBM','g0w0'], dfb.loc['VBM',series[1]], dfb.loc['VBM',series[-1]])
    vb_vac = np.piecewise(xx, [(xx <= 1.5), (xx > 1.5) * (xx < 2.5), (xx > 2.5)], [dfb.loc['VBM','g0w0'],dfb.loc['VBM',series[1]], dfb.loc['VBM',series[-1]]])
    ax.plot(xx, vb_vac, color=cb)
    ax.fill_between(xx, vb_vac, vb_vac-2, lw=0, color=cb, alpha=alpha)

    cb_vac = np.piecewise(xx, [(xx <= 1.5), (xx > 1.5) * (xx < 2.5), (xx > 2.5)], [dfb.loc['VBM','g0w0']+eg,dfb.loc['VBM',series[1]]+eg, dfb.loc['VBM',series[-1]]+eg])
    ax.plot(xx, cb_vac, color=cb)
    ax.fill_between(xx, cb_vac, cb_vac+2, lw=0, color=cb, alpha=alpha)
    
    for j in range(1,4):
        for orb in ['d1']:
            rect = patches.Rectangle((j-w/2,dfb.loc[orb,series[j-1]]+corr),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
            ax.add_patch(rect)
        ax.text(j,-4.6, r'{0}'.format(labels[j-1]), ha='center', fontsize=5.5)

    ax.set_xlim([xmin, xmax])
    ax.set_ylim([-7.0,-3.0])

    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    for orb in ['d1']:
        for j in range(1,3):
            ax.annotate('', xy=(j+w/2,dfb.loc[orb, series[j-1]]+corr),xytext=(j+1-w/2,dfb.loc[orb, series[j]]+corr), xycoords='data', arrowprops = props)
    yl = dfb.loc['VBM',series[0]]; yh = dfb.loc['VBM',series[0]]+eg
    ax.imshow([[1.,0.],[1.,0]], cmap = cm2, extent = [0, 3.5, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .2, aspect = "auto")

    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{vac}$ (eV)')
    return dfb

series = ['g0w0','pbe0','hse']
vac_ref = [vbm_vac_g0w0,vbm_vac_pbe0_22,vbm_vac_hse_55]
labels = [r'G$_0$W$_0$', 'PBE0', 'HSE']
dfa = plot_vbm(series, vac_ref, labels, ax0, corr=0.75)
dfb = plot_vac(series, vac_ref, labels, ax1, corr=0.75)

plt.savefig('cos_g0w0.pdf')
