#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
import seaborn as sns
import pandas as pd

fig_width = 16/2.54
fig_height = fig_width*1
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette("Paired")
cb = 'C0'
cd = 'C1'
fig = plt.figure()
gs = fig.add_gridspec(2,2, left=0.08, right=0.96, top=0.92, bottom=0.10, wspace=0.2)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])
ax2 = fig.add_subplot(gs[2])
ax3 = fig.add_subplot(gs[3])

eg = 2.9
vbm_vac_hse_55 = -6.52
vbm_vac_pbe0_22 = -6.23
alpha = 0.5
xx = np.linspace(0,1,400)
l0 = 0.05; w = 0.15; h = 0.01

df = pd.read_csv('cos_g0w0.dat', delim_whitespace=True, index_col=0)

def plot_vbm(series,vbm_vac_ref,labels,ax):
    dfa = df.copy()
    for i in series:
        dfa.loc['VBM',i] += vbm_vac_ref - dfa.loc['VBM_vac',i]
    for i in ['g0w0']:
        dfa[i] -= dfa.loc['VBM',i]
    for i in series:
        dfa[i] -= dfa.loc['VBM',i]

    ax.axhline(0, color=cb)
    ax.fill_between(xx, 0, -2, color=cb, alpha=alpha)
    ax.axhline(eg, color=cb)
    ax.fill_between(xx, eg, eg+2, color=cb, alpha=alpha)

    for orb in ['d1','d2','d3']:
        rect = patches.Rectangle((l0,dfa.loc[orb,'g0w0']),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)
        rect = patches.Rectangle((1/4+l0,dfa.loc[orb,series[0]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)
        rect = patches.Rectangle((2/4+l0,dfa.loc[orb,series[1]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)
        rect = patches.Rectangle((3/4+l0,dfa.loc[orb,series[2]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)

    ax.set_xlim([0,1])
    ax.set_ylim([-0.4,eg+0.4])
    ax.text(0+0.1,-0.05, r'{0}'.format(labels[0]), ha='center', transform=ax.transAxes)
    ax.text(1/4+0.1,-0.05, r'{0}'.format(labels[1]), ha='center', transform=ax.transAxes)
    ax.text(2/4+0.1,-0.05, r'{0}'.format(labels[2]), ha='center', transform=ax.transAxes)
    ax.text(3/4+0.1,-0.05, r'{0}'.format(labels[3]), ha='center', transform=ax.transAxes)

    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    ax.text(0.05,0.8, r'$Co_S^{-1}$, 36-atom', transform=ax.transAxes)
    for orb in ['d1','d3']:
        ax.annotate('', xy=(l0+w,dfa.loc[orb, 'g0w0']),xytext=(l0+1/4,dfa.loc[orb, series[0]]), xycoords='data', arrowprops = props)
        ax.annotate('', xy=(1/4+l0+w,dfa.loc[orb, series[0]]),xytext=(l0+2/4,dfa.loc[orb, series[1]]), xycoords='data', arrowprops = props)
        ax.annotate('', xy=(2/4+l0+w,dfa.loc[orb, series[1]]),xytext=(l0+3/4,dfa.loc[orb, series[2]]), xycoords='data', arrowprops = props)

    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{VBM}$ (eV)')
    return dfa

def plot_vac(series,vbm_vac_ref,labels,ax):
    dfb = df.copy()
    for i in ['g0w0']:
        for row in ['d1','d2','d3','VBM']:
            dfb.loc[row,i] -= dfb.loc['VBM',i] - dfb.loc['VBM_vac',i]
    for i in series:
        dfb.loc['VBM',i] -= dfb.loc['VBM_vac',i] - vbm_vac_ref
        for row in ['d1','d2','d3','VBM']:
            dfb.loc[row,i] -= dfb.loc['VBM',i] - vbm_vac_ref

    vb_vac = np.piecewise(xx, [xx <= 0.25, xx > 0.25], [dfb.loc['VBM','g0w0'], dfb.loc['VBM',series[0]]])
    ax.plot(xx, vb_vac, color=cb)
    ax.fill_between(xx, vb_vac, vb_vac-2, color=cb, alpha=alpha)

    cb_vac = np.piecewise(xx, [xx <= 0.25, xx > 0.25], [dfb.loc['VBM','g0w0']+eg, dfb.loc['VBM',series[0]]+eg])
    ax.plot(xx, cb_vac, color=cb)
    ax.fill_between(xx, cb_vac, cb_vac+2, color=cb, alpha=alpha)

    for orb in ['d1','d2','d3']:
        rect = patches.Rectangle((l0,dfb.loc[orb,'g0w0']),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)
        rect = patches.Rectangle((1/4+l0,dfb.loc[orb,series[0]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)
        rect = patches.Rectangle((2/4+l0,dfb.loc[orb,series[1]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)
        rect = patches.Rectangle((3/4+l0,dfb.loc[orb,series[2]]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
        ax.add_patch(rect)

        ax.set_xlim([0,1])
        ax.set_ylim([-6.8,-6.8+eg+1.0])
    ax.text(0+0.1,-0.05, r'{0}'.format(labels[0]), ha='center', transform=ax.transAxes)
    ax.text(1/4+0.1,-0.05, r'{0}'.format(labels[1]), ha='center', transform=ax.transAxes)
    ax.text(2/4+0.1,-0.05, r'{0}'.format(labels[2]), ha='center', transform=ax.transAxes)
    ax.text(3/4+0.1,-0.05, r'{0}'.format(labels[3]), ha='center', transform=ax.transAxes)
    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    for orb in ['d1','d3']:
        ax.annotate('', xy=(l0+w,dfb.loc[orb, 'g0w0']),xytext=(l0+1/4,dfb.loc[orb, series[0]]), xycoords='data', arrowprops = props)
        ax.annotate('', xy=(1/4+l0+w,dfb.loc[orb, series[0]]),xytext=(l0+2/4,dfb.loc[orb, series[1]]), xycoords='data', arrowprops = props)
        ax.annotate('', xy=(2/4+l0+w,dfb.loc[orb, series[1]]),xytext=(l0+3/4,dfb.loc[orb, series[2]]), xycoords='data', arrowprops = props)
    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{vac}$ (eV)')
    return dfb

dfa = plot_vbm(['hse','hse_18','pbe'], vbm_vac_hse_55, ['$G_0W_0$','HSE (5)', 'HSE (18)','PBE'],ax0)
dfb = plot_vac(['hse','hse_18','pbe'], vbm_vac_hse_55, ['$G_0W_0$','HSE (5)', 'HSE (18)','PBE'],ax1)

dfa = plot_vbm(['pbe0','pbe0_g','pbe'], vbm_vac_pbe0_22, ['$G_0W_0$','PBE0 (4)', 'PBE0 (22)','PBE'],ax2)
dfb = plot_vac(['pbe0','pbe0_g','pbe'], vbm_vac_pbe0_22, ['$G_0W_0$','PBE0 (4)', 'PBE0 (22)','PBE'],ax3)

plt.savefig('cos_g0w0.pdf')
