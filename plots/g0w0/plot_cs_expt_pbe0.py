#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.65
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'hatch.linewidth': 0.5,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'xtick.bottom': False,
          'ytick.right': False,
          'axes.spines.top': False,
          'axes.spines.bottom': False,
          'axes.spines.right': False,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette("Paired")
cb = 'C0'
cd = 'C1'
cd_rel = 'C0'
fig = plt.figure()
gs = fig.add_gridspec(1,2,left=0.12, width_ratios=[1.5,1],right=0.88, top=0.96, bottom=0.10, wspace=0.2)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])

eg = 2.9
vbm_vac_pbe0_22 = -6.23
vbm_vac_g0w0 = -6.18
vbm_vac_expt = vbm_vac_pbe0_22
alpha = 0.4
xx = np.linspace(0,6,1000)
w = 0.5; h = 0.01
xmin = 0.5; xmax = 3-xmin
cm_hse = cm.Greens
cm_pbe0 = cm.Blues
cm_be = cm.Blues
ymin = -6.5; ymax = -3.0

df = pd.read_csv('cs_g0w0.dat', delim_whitespace=True, index_col=0)

def plot_vac(series,vbm_vac_ref,labels,ax,corr=0,relax=0):
    dfb = df.copy()
    for ind, i in enumerate(series):
        dfb.loc['VBM',i] -= dfb.loc['VBM_vac',i] - vbm_vac_ref[ind]
        for row in ['d1u', 'd1d', 'VBM']:
            dfb.loc[row,i] -= dfb.loc['VBM',i] - vbm_vac_ref[ind]
    
    vb_vac = np.piecewise(xx, [(xx <= 1.5), (xx > 1.5)], [dfb.loc['VBM','expt'], dfb.loc['VBM',series[-1]]])
    ax.plot(xx, vb_vac, color=cb)
    ax.fill_between(xx, vb_vac, vb_vac-2, lw=0, color=cb, alpha=0.1)
    ax.fill_between(xx, vb_vac, vb_vac-2, lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)

    cb_vac = np.piecewise(xx, [(xx <= 1.5), (xx > 1.5)], [dfb.loc['VBM','expt']+eg, dfb.loc['VBM',series[-1]]+eg])
    ax.plot(xx, cb_vac, color=cb)
    ax.fill_between(xx, cb_vac, cb_vac+2, lw=0, color=cb, alpha=0.1)
    ax.fill_between(xx, cb_vac, cb_vac+2, lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)
    
    for j in range(len(series)):
        for orb in ['d1u', 'd1d']:
            _corr = 0 if series[j] == 'expt' else corr
            rect = patches.Rectangle((j+1-w/2,dfb.loc[orb,series[j]]+_corr),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.8)
            ax.add_patch(rect)
        ax.text(j+1,-5.6, r'{0}'.format(labels[j]), ha='center')

        if abs(relax) > 1E-2 and series[j] != 'expt':
            for orb in ['d1u','d1d']:
                rect = patches.Rectangle((j+1-w/2,dfb.loc[orb,series[j]]+corr+relax),w,h+0.01, ls='--', linewidth=0.5, edgecolor=cd_rel, facecolor=cd_rel, alpha=0.8)
                ax.add_patch(rect)
                props = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
                arrow_params = dict(length_includes_head=False, lw=0.5, ls='-', head_width=0.03, head_length=0.04, shape='full', overhang=0.5, color='0.4')
                ax.arrow(j+1,dfb.loc[orb,series[j]]+corr, 0, relax, **arrow_params)
                relax *= -1
        ax.text(j+1,-5.6, r'{0}'.format(labels[j]), ha='center')

    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])
    
    props = dict(arrowstyle="-", color="lightgrey", lw = 0.5, ls = '--')
    _ctl = ctl + vbm_22
    for orb in ['d1u', 'd1d']:
        for j in range(1,2):
            print(_ctl) 
            ax.annotate('', xy=(j+w/2,dfb.loc[orb, series[j-1]]),xytext=(j+1-w/2,dfb.loc[orb, series[j]]+corr+relax), xycoords='data', arrowprops = props)
            ax.annotate('', xy=(j+1+w/2,dfb.loc[orb, series[j]]+corr+relax),xytext=(j+1+w/2+0.3,_ctl), xycoords='data', arrowprops = props)
            relax *= -1
            _ctl = ctl2 + vbm_22

    yl = dfb.loc['VBM','expt']; yh = dfb.loc['VBM','expt']+eg
    ax.imshow([[0.5,0],[1.,0]], cmap = cm_pbe0, extent = [0, 1.5, yl, yh], interpolation = "bilinear", vmin=0, vmax=5.5, alpha = .6, aspect = "auto")

    ax.get_xaxis().set_visible(False)
    ax.set_ylabel(r'$E$ $-$ $E_{vac}$ (eV)')

    ax.text(1, dfb.loc['d1u','expt']-0.2, r'$\uparrow$ $p_z+d_{z^2}$',ha='center',fontsize=6)
    ax.text(1, dfb.loc['d1d','expt']-0.2, r'$\downarrow$ $p_z+d_{xy,x^2-y^2}$',ha='center',fontsize=6)
    props = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
    ax.annotate("",xy=(2.5,dfb.loc['VBM','pbe0']), xycoords=ax.transData, xytext=(2.5,dfb.loc['VBM','pbe0']+eg),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.2", lw=0.5, shrinkA=1, shrinkB=1, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
    ax.text(2.5+0.15,dfb.loc['VBM','pbe0']+eg/2-0.5,r'$\alpha=0.22$',props,va='center',rotation=90, fontsize=6)
    ax.text(0.5,-0.09,r'C$_S^-$', transform=ax.transAxes)
    return dfb

hatch = '////'
series = ['expt','pbe0']
vac_ref = [vbm_vac_expt,vbm_vac_pbe0_22]
labels = [r'Expt.', r'PBE0 ($\alpha^{kopt}$)']

dfe = pd.read_csv('cs_eform.dat', delim_whitespace=True)
dfe['eform'] -= dfe.iloc[0]['eform']
dfe['eform'] += 1.6
vbm_5 = -5.78
vbm_22 = -6.23
yy = np.linspace(vbm_22-vbm_22,vbm_22-vbm_22+eg,10)
ax1.spines['bottom'].set_visible(True)
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(True)
ax1.spines['left'].set_visible(False)
ax1.get_xaxis().set_ticklabels([])
ax1.set_ylim([ymin-vbm_22,ymax-vbm_22])
ax1.set_xlim([0.2,1.75])

ax1.plot(-yy+dfe.iloc[1]['eform']+vbm_5-vbm_22,yy, color='C0', lw=1)
ax1.plot(dfe.iloc[0]['eform']+0*yy, yy, color='C0', lw=1)
ax1.plot(-2*(yy-vbm_5+vbm_22)+dfe.iloc[2]['eform'], yy, color='C0', lw=1)
ax1.invert_xaxis()

ctl = dfe.iloc[1]['eform']+vbm_5-vbm_22-dfe.iloc[0]['eform']
ax1.scatter(dfe.iloc[0]['eform'],ctl,color='C1', zorder=10)
ctl2 = dfe.iloc[2]['eform']+vbm_5-vbm_22-dfe.iloc[1]['eform']
ax1.scatter(dfe.iloc[2]['eform']+2*(vbm_5-vbm_22-ctl2),ctl2,color='C1', zorder=10)

xx2 = np.linspace(0,5,10)
ax1.fill_between(xx2,0,-1,lw=1., fc=cb, ec=cb, alpha=0.1)
ax1.fill_between(xx2,0,-1,lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)
ax1.fill_between(xx2,eg,eg+1,lw=1., fc=cb, ec=cb, alpha=0.1)
ax1.fill_between(xx2,eg,eg+1,lw=0., fc='none', ec=cb, hatch=hatch, rasterized=True, alpha=0.9)
ax1.plot(xx2,0*xx2,color=cb)
ax1.plot(xx2,eg+0*xx2,color=cb)
ax1.set_xlabel(r'$E_{form}$')
ax1.yaxis.set_label_position('right')
ax1.yaxis.tick_right()
ax1.set_ylabel(r'$\mu$ (eV)')
ax1.text(1.55,1.0,r'$q=0$',rotation='90')
ax1.text(1.5,1.65,r'$q=-1$',rotation='42')
ax1.text(0.7,2.4,r'$q=-2$',rotation='21')
ax1.annotate("",xy=(dfe.iloc[0]['eform'],ctl),xytext=(dfe.iloc[0]['eform']+0.45,ctl), xycoords=ax1.transData, textcoords=ax1.transData,arrowprops=dict(arrowstyle="-", color="0.2", lw=0.5, shrinkA=0, shrinkB=0, ls='dashed', patchA=None, patchB=None))
ax1.annotate("",xy=(dfe.iloc[2]['eform']+2*(vbm_5-vbm_22-ctl2),ctl2),xytext=(dfe.iloc[2]['eform']+2*(vbm_5-vbm_22-ctl2)+1.25,ctl2), xycoords=ax1.transData, textcoords=ax1.transData,arrowprops=dict(arrowstyle="-", color="0.2", lw=0.5, shrinkA=0, shrinkB=0, ls='dashed', patchA=None, patchB=None))
ax1.text(1.3,0.62,r'PBE0 ($\alpha^{kopt}$)')

dfb = plot_vac(series, vac_ref, labels, ax0, corr=0.3, relax=0.1)

plt.savefig('cs_expt_pbe0.png', dpi=600)
plt.savefig('cs_expt_pbe0.pdf', dpi=600)
