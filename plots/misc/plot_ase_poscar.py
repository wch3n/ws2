#!/usr/bin/env python3

import matplotlib.pyplot as plt
from ase.visualize import plot, view
from ase.io.vasp import read_vasp

a = read_vasp('CONTCAR')
plt.axis('off')
plot.plot_atoms(a, rotation='0x')
plt.savefig('fig1.pdf')
a.center()
plot.plot_atoms(a, rotation='-90x')
plt.savefig('fig2.pdf')
