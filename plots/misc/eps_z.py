#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.ticker import MultipleLocator

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 7,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Set2')

eps = np.genfromtxt('eps_z.dat')

fig = plt.figure()
gs = fig.add_gridspec(1,1,left=0.12, right=0.95, top=0.92, bottom=0.12, hspace=0.2)
ax0 = fig.add_subplot(gs[0])
#ax1 = fig.add_subplot(gs[1])

eps_xy = eps[:,1]
eps_z =  eps[:,2]
L = eps[:,0]
s = 6.15
z_ml = 3.11+8

def calc_eps_slab(eps):
    return 1 + L/s*(eps-1)

def calc_slope(x,y):
    z = np.polyfit(x, y, 1)
    return z  

ax0.plot(1/L, eps_xy, 'o', c='C0',lw=0.7, label=r'$\varepsilon_{\parallel}$', clip_on=False)
#ax0.plot(1/L, eps_z, '-o')
ax0.plot(1/L, eps_z, 'o', c='C1', lw=0.7, label=r'$\varepsilon_{\bot}$', clip_on=False)
ax0.plot(1/L, 1/eps_z, 's', c='C1', lw=0.7, label=r'$\varepsilon^{-1}_{\bot}$', clip_on=False)

ax0.axvline(x=1/s, ls='--', lw=0.5, c='C2')
ax0.axvline(x=1/z_ml, ls='--', lw=0.5, c='C2')

ax0.text(1/z_ml, 14.9, r'z$_{ML}$', ha='center')
ax0.text(1/s, 14.9, r'z$_{bulk}$', ha='center')

ax0.legend()

eps_slab_xy = calc_eps_slab(eps_xy)
#eps_slab_z  = calc_eps_slab(eps_z)
eps_slab_z2 = 1/calc_eps_slab(1/eps_z)

#print(eps_slab_xy[-1], eps_slab_z[-1], eps_slab_z2[-1])

z = calc_slope(1/L[3:],eps_xy[3:])
p = np.poly1d(z)
eps_fit_xy = z[0]/s+1
izz = np.linspace(0,0.2,100)
ax0.plot(izz, p(izz), 'C0', ls='--', lw=0.5)
ax0.scatter(1/z_ml, p(1/z_ml), marker='x', s=16, c='C2', zorder=99)
ax0.scatter(1/s, p(1/s), marker='x', s=16, c='C2', zorder=99)

z = calc_slope(1/L[3:],1/eps_z[3:])
p = np.poly1d(z)
eps_fit_z = 1/(z[0]/s+1)
ax0.plot((izz), p(izz), 'C1', ls='--', lw=0.5)
ax0.plot(izz, 1/p(izz), 'C1', ls='--', lw=0.5)
ax0.scatter(1/z_ml, 1/p(1/z_ml), marker='x', s=16, c='C2', zorder=99)
ax0.scatter(1/s, 1/p(1/s), marker='x', s=16, c='C2', zorder=99)

ax0.set_xlabel(r'$1/L_Z$ ($\AA^{-1}$)')
ax0.set_ylabel('Supercell (inverse) dielectric constant')

ax0.yaxis.set_major_locator(MultipleLocator(2))
ax0.yaxis.set_minor_locator(MultipleLocator(1))

ax0.set_xlim([0.0,1/L[0]])
ax0.set_ylim([0,14.5])

plt.savefig('eps_z.pdf')
