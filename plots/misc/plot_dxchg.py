#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches

fig_width = 8.5/2.54
fig_height = fig_width*0.7
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

sns.set_palette('Set2')

fig = plt.figure()
ax0 = fig.subplots(1,1)

vs_dxchg = np.genfromtxt('vs_DXCHG.dat')
cos_dxchg = np.genfromtxt('cos_DXCHG.dat')

vs_dxchg[:,0] -= vs_dxchg[-1,0]/2
cos_dxchg[:,0] -= cos_dxchg[-1,0]/2

ax0.plot(vs_dxchg[:,0], vs_dxchg[:,1], lw=0.8, ls='--')
ax0.plot(cos_dxchg[:,0], cos_dxchg[:,1], lw=0.8, ls='-')

ax0.set_xlabel(r'$x$ ($\AA$)')
ax0.set_ylabel(r'$\delta$ $\rho$ ($e$/$\AA^{-1}$)')

ax0.text(0.4,0.9, r'V$_S^{-1}$', transform=ax0.transAxes)
ax0.text(0.45,0.2, r'Co$_S^{-1}$', transform=ax0.transAxes)

ax0.set_xlim([vs_dxchg[0,0],vs_dxchg[-1,0]])

plt.tight_layout()
plt.savefig('dxchg.pdf')
