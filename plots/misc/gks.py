#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
import seaborn as sns
import pandas as pd

fig_width = 16/2.54
fig_height = fig_width*0.4
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
cb = 'C0'
cd = 'C1'

sns.set_palette('Paired')

df = pd.read_csv('gks.dat', delim_whitespace=True, index_col=0)
for label in df.keys():
    df.loc['vs',label] -= df.loc['VBM',label]
    df.loc['CBM',label] -= df.loc['VBM',label]
    df.loc['VBM',label] -= df.loc['VBM',label]
    df.loc['cos',label] -= df.loc['VBM2',label]
    df.loc['VBM2',label] -= df.loc['VBM2',label]

for label in ['hse_opt_c','hse_25_c','pbe_c']:
    df.loc['vs',label] += 0.14
    df.loc['cos',label] -= 0.14

fig = plt.figure()
gs = fig.add_gridspec(1,2, left=0.06, right=0.98, top=0.92, bottom=0.15, wspace=0.2)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])

rect = patches.Rectangle((0,-0.4),1,0.4, linewidth=0.5, edgecolor=cb, facecolor=cb, alpha=0.7, fill=False, hatch='///////')
ax0.add_patch(rect)
rect = patches.Rectangle((0,df.loc['CBM','hse_opt']),1,2, linewidth=0.5, edgecolor=cb, facecolor=cb, alpha=0.7, fill=False, hatch='///////')
ax0.add_patch(rect)

l0 = 0.15
w = 0.2
h = 0.02

orb = 'vs'
for label in ['hse_25','hse_opt','pbe']:
    rect = patches.Rectangle((l0,df.loc[orb,label]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.7)
    ax0.add_patch(rect)
for label in ['hse_25_c','hse_opt_c','pbe_c']:
    rect = patches.Rectangle((l0+0.4,df.loc[orb,label]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.7)
    ax0.add_patch(rect)

ax0.set_xlim([0,1])
ax0.set_ylim([-0.4,df.loc['CBM','hse_opt']+0.4])

props = dict(arrowstyle="-", color="gray", lw = 0.6, ls = '--')
ax0.annotate('', xy=(l0+w,df.loc[orb, 'pbe']),xytext=(l0+0.4,df.loc[orb, 'pbe_c']), xycoords='data', arrowprops = props)
ax0.annotate('', xy=(l0+w,df.loc[orb, 'hse_opt']),xytext=(l0+0.4,df.loc[orb, 'hse_opt_c']), xycoords='data', arrowprops = props)
ax0.annotate('', xy=(l0+w,df.loc[orb, 'hse_25']),xytext=(l0+0.4,df.loc[orb, 'hse_25_c']), xycoords='data', arrowprops = props)

ax0.get_xaxis().set_visible(False)
ax0.set_ylabel(r'$E$ $-$ $E_{VBM}$ (eV)')
ax0.text(0.15,-0.1, r'$V_S^0$@$R_0$', transform = ax0.transAxes)
ax0.text(0.60,-0.1, r'$V_S^{-1}$@$R_0$', transform = ax0.transAxes)
ax0.text(0.80, df.loc[orb, 'hse_25_c'], 'HSE (0.25)', va='center')
ax0.text(0.80, df.loc[orb, 'hse_opt_c'], 'HSE (0.10)', va='center')
ax0.text(0.80, df.loc[orb, 'pbe_c'], 'PBE', va='center')
#

rect = patches.Rectangle((0,-0.4),1,0.4, linewidth=0.5, edgecolor=cb, facecolor=cb, alpha=0.7, fill=False, hatch='///////')
ax1.add_patch(rect)
rect = patches.Rectangle((0,df.loc['CBM','hse_opt']),1,2, linewidth=0.5, edgecolor=cb, facecolor=cb, alpha=0.7, fill=False, hatch='///////')
ax1.add_patch(rect)
orb = 'cos'
for label in ['hse_25','hse_opt','pbe']:
    rect = patches.Rectangle((l0,df.loc[orb,label]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.7)
    ax1.add_patch(rect)
for label in ['hse_25_c','hse_opt_c','pbe_c']:
    rect = patches.Rectangle((l0+0.4,df.loc[orb,label]),w,h, linewidth=0.5, edgecolor=cd, facecolor=cd, alpha=0.7)
    ax1.add_patch(rect)

ax1.set_xlim([0,1])
ax1.set_ylim([-0.4,df.loc['CBM','hse_opt']+0.4])

ax1.annotate('', xy=(l0+w,df.loc[orb, 'pbe']),xytext=(l0+0.4,df.loc[orb, 'pbe_c']), xycoords='data', arrowprops = props)
ax1.annotate('', xy=(l0+w,df.loc[orb, 'hse_opt']),xytext=(l0+0.4,df.loc[orb, 'hse_opt_c']), xycoords='data', arrowprops = props)
ax1.annotate('', xy=(l0+w,df.loc[orb, 'hse_25']),xytext=(l0+0.4,df.loc[orb, 'hse_25_c']), xycoords='data', arrowprops = props)

ax1.get_xaxis().set_visible(False)
#ax1.set_ylabel(r'$E$ $-$ $E_{VBM}$ (eV)')
ax1.text(0.15,-0.1, r'$Co_S^0$@$R_0$', transform = ax1.transAxes)
ax1.text(0.60,-0.1, r'$Co_S^{+1}$@$R_0$', transform = ax1.transAxes)
ax1.text(0.80, df.loc[orb, 'hse_25_c'], 'HSE (0.25)', va='center')
ax1.text(0.80, df.loc[orb, 'hse_opt_c'], 'HSE (0.05)', va='center')
ax1.text(0.80, df.loc[orb, 'pbe_c'], 'PBE', va='center')


plt.savefig('gks.pdf')
