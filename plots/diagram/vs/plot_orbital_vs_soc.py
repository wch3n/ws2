#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches

def add_label(df, iloc, delta_y, show_text=False, text_shift=0):
    x1 = 0.35; y1 = df.iloc[iloc]['energy']; x2 = 0.42; y2 = df.iloc[iloc]['energy']+delta_y
    connectionstyle = "arc,angleA=0,angleB=0,armA=-10,armB=5,rad=0"
    props = dict(arrowstyle="-", color="gray", lw = 0.6, ls = '--', shrinkA=1, shrinkB=1, patchA=None, patchB=None, connectionstyle=connectionstyle)
    ax0.annotate('', xy=(x1,y1), xycoords='data', xytext=(x2,y2), textcoords='data', arrowprops = props, horizontalalignment='left')
    if show_text:
        bbox = dict(ec='none',fc='w',pad=2)
        ax0.text(x2+0.02,y2-0.04+text_shift,r'${0}$'.format(df.iloc[iloc]['char']),bbox=bbox, zorder=99)

def add_label_r(df, iloc, delta_y):
    x1 = 0.55; y1 = df.iloc[iloc]['energy'] - delta_y; x2 = 0.65; y2 = df.iloc[iloc]['energy']
    connectionstyle = "arc,angleA=0,angleB=0,armA=-5,armB=10,rad=0"
    props = dict(arrowstyle="-", color="gray", lw = 0.6, ls = '--', shrinkA=1, shrinkB=1, patchA=None, patchB=None, connectionstyle=connectionstyle)
    ax0.annotate('', xy=(x1,y1), xycoords='data', xytext=(x2,y2), textcoords='data', arrowprops = props, horizontalalignment='left')

fig_width = 12/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 7,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

fig = plt.figure(constrained_layout=True)
ax0 = fig.subplots(1,1)

df = pd.read_csv('vs_0_orbital_soc.dat', delim_whitespace=True)
df['energy'] -= df.iloc[-1]['energy']

l0 = 0.05
w0 = 0.30
for i in [0]:
    h = 0.5
    rect = patches.Rectangle((l0-0.02,df.iloc[i]['energy']), w0+0.04, h, linewidth=0.5, edgecolor='C0', facecolor='C0', alpha=0.2)
    ax0.add_patch(rect)   
for i in [1,2,3,4]:
    h = 0.03
    rect = patches.Rectangle((l0,df.iloc[i]['energy']-h/2), w0, h, linewidth=0.5, edgecolor='C0', facecolor='none')
    ax0.add_patch(rect)
for i in [-1]:
    h = 0.3
    rect = patches.Rectangle((l0-0.02,df.iloc[i]['energy']), w0+0.04, -h, linewidth=0.5, edgecolor='C0', facecolor='C0', alpha=0.2)
    ax0.add_patch(rect)   

df = pd.read_csv('vs_1-_orbital_soc.dat', delim_whitespace=True)
df['energy'] -= df.iloc[-1]['energy']
df.loc[1:4,'energy'] += 0.5
l0 = 0.65
w0 = 0.30
for i in [0]:
    h = 0.5
    rect = patches.Rectangle((l0-0.02,df.iloc[i]['energy']), w0+0.04, h, linewidth=0.5, edgecolor='C0', facecolor='C0', alpha=0.2)
    ax0.add_patch(rect)   
for i in [1,2,3]:
    h = 0.03
    rect = patches.Rectangle((l0,df.iloc[i]['energy']-h/2), w0, h, linewidth=0.5, edgecolor='C0', facecolor='none')
    ax0.add_patch(rect)
for i in [4]:
    h = 0.015
    rect = patches.Rectangle((l0,df.iloc[i]['energy']-h/2), w0, h, linewidth=0.5, edgecolor='C0', facecolor='C0')
    ax0.add_patch(rect)
for i in [-1]:
    h = 0.3
    rect = patches.Rectangle((l0-0.02,df.iloc[i]['energy']), w0+0.04, -h, linewidth=0.5, edgecolor='C0', facecolor='C0', alpha=0.2)
    ax0.add_patch(rect)   

ax0.text(0.2,1.02,r'$V_S^0$',transform=ax0.transAxes)
ax0.text(0.8,1.02,r'$V_S^{-1}$',transform=ax0.transAxes)

ax0.set_xlim([0,1])
ax0.set_ylim([-0.3,2.6])
ax0.set_ylabel('Energy (eV)')
plt.tick_params(top=False, bottom=False, left=True, right=False, labelleft=True, labelbottom=False)
plt.savefig('vs_orbital_soc.pdf')
