#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from ase.io.vasp import read_vasp
from ase import Atoms
from ase.visualize import plot

def connect(x1, x2, y1, y2, shrinkA=1, shrinkB=1, armA=-10, armB=10):
    connectionstyle = "arc,angleA=0,angleB=0,armA={0},armB={1},rad=0".format(armA, armB)
    props = dict(arrowstyle="-", color="gray", lw = 0.6, ls = '--', shrinkA=shrinkA, shrinkB=shrinkB, patchA=None, patchB=None, connectionstyle=connectionstyle)
    ax.annotate('', xy=(x1,y1), xycoords='data', xytext=(x2,y2), textcoords='data', arrowprops = props, horizontalalignment='left')

def plot_cow3(poscar,dy,ax):
    s = read_vasp(poscar)
    co = s[-1]; w1 = s[9]; w2 = s[10]; w3 = s[15]
    co.position += [0,dy,0]
    cow3 = Atoms('CoW3', positions=(co.position,w1.position,w2.position,w3.position))
    plot.plot_atoms(cow3, offset=[-5.5,-7.5,0], radii = [0.95,0.9,0.9,0.9], ax=ax)
    ax.axis('off')

fig_width = 16/2.54
fig_height = fig_width*0.6
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 7,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

props = dict(arrowstyle= '->', color='C0', lw=0.8, ls='-')

fig = plt.figure()
#gs = fig.add_gridspec(ncols=1,nrows=2, height_ratios=[1,5], left=0.08, right=0.95, top=0.95, bottom=0.04, hspace=0)
gs = fig.add_gridspec(ncols=1,nrows=1, left=0.08, right=0.95, top=0.95, bottom=0.04, hspace=0)

ax = fig.add_subplot(gs[0])
#upper_grid = gs[0].subgridspec(ncols=3, nrows=1)
#ax0 = fig.add_subplot(upper_grid[0]) 
#ax1 = fig.add_subplot(upper_grid[1]) 
#ax2 = fig.add_subplot(upper_grid[2]) 

w0=0.25
bbox = dict(ec='none',fc='none',pad=1)

def draw_diagram(df,l0,corr):
    for i, r in df.iterrows():
        if r['char'] == 'CBM':
            h = 0.5 
            rect = patches.Rectangle((l0-0.02,r['energy']), w0, h, linewidth=0.5, edgecolor='C0', facecolor='C0', alpha=0.3)
            ax.add_patch(rect)   
        elif r['char'] == 'VBM':
            h =  1.0
            rect = patches.Rectangle((l0-0.02,r['energy']), w0, -h, linewidth=0.5, edgecolor='C0', facecolor='C0', alpha=0.3)
            ax.add_patch(rect)   
        elif r['occ'] == 0:
            if r['spin'] == 'up':
                w = 0.09; l = l0
            elif r['spin'] == 'dn':
                w = 0.09; l = l0 + w + 0.03
            else:
                w = 0.12; l = l0+0.04
            h = 0.02
            r['energy'] += corr
            rect = patches.Rectangle((l,r['energy']-h/2), w, h, linewidth=0.5, edgecolor='C0', facecolor='none')
            ax.add_patch(rect)
        elif r['occ'] > 0:
            if r['spin'] == 'up':
                w = 0.09; l = l0
            elif r['spin'] == 'dn':
                w = 0.09; l = l0 + w + 0.03
            else:
                w = 0.12; l = l0+0.04
            h = 0.018
            r['energy'] += corr
            if r['band'] > 0:
                rect = patches.Rectangle((l,r['energy']-h/2), w, h, linewidth=0.5, edgecolor='C0', facecolor='C0')
                ax.add_patch(rect)
            else:
                h = 0.03
                rect = patches.Rectangle((l,r['energy']-h/2), w, h, linewidth=0.3, edgecolor='C0', facecolor='C0', ls='--', alpha=0.3)
                ax.add_patch(rect)
            if r['occ'] == 1 and r['spin'] == 'up':
                ax.arrow(l+w/2,r['energy']-0.05, 0,  0.12, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
            if r['occ'] == 1 and r['spin'] == 'dn':
                ax.arrow(l+w/2,r['energy']+0.05, 0, -0.12, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
            if r['occ'] == 2:
                ax.arrow(l+w/2-0.005,r['energy']-0.05, 0, 0.14, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
                ax.arrow(l+w/2+0.005,r['energy']+0.06, 0, -0.14, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
            if r['occ'] == 4:
                ax.arrow(l+w/2-0.03,r['energy']-0.05, 0, 0.14, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
                ax.arrow(l+w/2-0.02,r['energy']+0.06, 0, -0.14, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
                ax.arrow(l+w/2+0.03,r['energy']-0.05, 0, 0.14, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
                ax.arrow(l+w/2+0.04,r['energy']+0.06, 0, -0.14, color='C0', shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.8)
        if r['label'] != 0 and r['band'] != -1:
            #ax.text(l+w/2-0.025,r['energy']+r['label']*0.15,r'${0}$'.format(r['char']),bbox=bbox, zorder=99)
            ax.text(l+w/2-0.05,r['energy']+r['label']*0.15,r'${0}$'.format(r['char']),bbox=bbox, ha='left',zorder=99)
        elif r['band'] == -1:
            if r['label'] == 0:
                ax.text(l+w/2+0.05,r['energy']-0.03,r'${0}$'.format(r['char']),bbox=bbox, zorder=99)
            else:
                ax.text(l+w/2-0.025,r['energy']+r['label']*0.15,r'${0}$'.format(r['char']),bbox=bbox, zorder=99)

corr = 0.30
dfp = pd.read_csv('ybs_1+_orbital.dat', delim_whitespace=True)
dfp['energy'] -= dfp.iloc[-1]['energy']
draw_diagram(dfp,0.05,-corr)

dfo = pd.read_csv('ybs_0_orbital.dat', delim_whitespace=True)
dfo['energy'] -= dfo.iloc[-1]['energy']
draw_diagram(dfo,0.35,0.)

dfn = pd.read_csv('ybs_1-_orbital.dat', delim_whitespace=True)
dfn['energy'] -= dfn.iloc[-1]['energy']
draw_diagram(dfn,0.65,corr)

#ax.text(-0.6,0.7,r'$Co_S^{+1}$ $(C_s) $',transform=ax0.transAxes)
ax.text(0.18,1.02,r'$Yb_S^{+1}$ ', ha='center',transform=ax.transAxes)
ax.text(0.50,1.02,r'$Yb_S^{0}$ ', ha='center',transform=ax.transAxes)
ax.text(0.82,1.02,r'$Yb_S^{-1}$', ha='center',transform=ax.transAxes)
ax.set_xlim([0,0.91])
ax.set_ylim([-0.65,2.5])
ax.set_ylabel('Energy (eV)')

#plot_cow3('POSCAR.1+', -0.5, ax0)
#plot_cow3('POSCAR.0',  -0.3,ax1)
#plot_cow3('POSCAR.1-', 0, ax2)

xl = 0.27; xr = 0.38
connect(xl, xr, dfp.iloc[0+10]['energy']-corr, dfo.iloc[1]['energy'])
connect(xl, xr, dfp.iloc[1+10]['energy']-corr, dfo.iloc[2]['energy'])
connect(xl, xr, dfp.iloc[2+10]['energy']-corr, dfo.iloc[3]['energy'])
connect(xl, xr, dfp.iloc[8+10]['energy']-corr, dfo.iloc[9]['energy'])

xl = 0.52; xr = 0.64
connect(xl, xr, dfo.iloc[1]['energy'], dfn.iloc[1]['energy']+corr)
connect(xl, xr, dfo.iloc[2]['energy'], dfn.iloc[2]['energy']+corr)
connect(xl, xr, dfo.iloc[3]['energy'], dfn.iloc[3]['energy']+corr)
connect(xl, xr, dfo.iloc[9]['energy'], dfn.iloc[9]['energy']+corr)

ax.get_xaxis().set_visible(False)
plt.savefig('ybs_orbital.pdf')
