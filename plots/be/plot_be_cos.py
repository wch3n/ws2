#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 14/2.54
fig_height = fig_width*0.75
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 10,
          'axes.labelweight': 'normal',
          'font.size': 10,
          'font.weight': 'normal',
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 5.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Paired')

df_cos = pd.read_csv('be_cos_pbe0.dat', delim_whitespace=True, comment='#')
df_vs = pd.read_csv('be_vs_pbe0.dat', delim_whitespace=True, comment='#')
df_cs = pd.read_csv('be_cs_pbe0.dat', delim_whitespace=True, comment='#')
df_cow = pd.read_csv('be_cow_pbe0.dat', delim_whitespace=True, comment='#')
fig = plt.figure()
gs = fig.add_gridspec(1,1, left=0.12, right=0.80, top=0.9, bottom=0.13, wspace=0.04)
ax1 = fig.add_subplot(gs[0])
cfill = 'azure'

def plot_vbm(df,xlim,ax,annotate=True):
    ax.plot(df['alpha'], df['vbm'],lw=1,ls='-',c='C0')
    ax.plot(df['alpha'], df['vbm']+df['eg'],lw=1,ls='-',c='C0')
    ax.fill_between(df['alpha'],df['vbm'],-7,color=cfill)
    ax.fill_between(df['alpha'],0,df['vbm']+df['eg'],color=cfill)

    ax.set_xlim([0,xlim])
    ax.set_ylim([yl,yh])
    ax.set_ylabel(r'$E - E_{vac}$ (eV)')
    ax.set_xlabel(r'$\alpha$')
    xtick = 0.1 if xlim > 0.4 else 0.05
    ax.xaxis.set_major_locator(MultipleLocator(xtick))
    ax.xaxis.set_minor_locator(MultipleLocator(xtick/2))
    ax.yaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_minor_locator(MultipleLocator(0.25))
    if annotate:
        props_side = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
        ax.annotate("",xy=(xlim,df['vbm'].iloc[-1]), xycoords=ax.transData, xytext=(xlim,df['vbm'].iloc[-1]+df['eg'].iloc[-1]),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.4", lw=0.5, shrinkA=2, shrinkB=2, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
        ax.text(xlim*1.05,df['vbm'].iloc[-1]+df['eg'].iloc[-1]/2,r'Ref. band gap (w/o SOC)', props_side,va='center',rotation=90, fontsize=8)

def plot_def(df, namedef, color, xlim, aopt, ax, extrapolate=True):
    cmap = cm.Oranges
    df2 = df.copy()
    n1, n2 = namedef[0], namedef[1] 
    ax.scatter(df2['alpha'], df2[n1],marker=".",c=color,clip_on=False)
    ax.plot(df2['alpha'], df2[n1],c=color,ls='-',lw=1)
    ax.scatter(df2['alpha'], df2[n2],marker=".",c=color,clip_on=False)
    ax.plot(df2['alpha'], df2[n2],c=color,ls='-',lw=1)

    if extrapolate:
        df_fit = df2[df2[n1] < 0]
        xmin, xmax = df_fit.alpha.min(), df_fit.alpha.max()
        z0 = np.polyfit(df_fit['alpha'], df_fit[n1], 1)
        p = np.poly1d(z0)
        _nan =  df2[np.isnan(df2[n1])]['alpha'].values
        if len(_nan) > 0:
            for x0 in _nan:
                if x0 < xmin:
                    xx = np.linspace(x0,xmin,10)
                elif x0 > xmax:
                    xx = np.linspace(xmax,x0,10)
                ax.plot(xx,p(xx),c=color, ls='--', lw=1, zorder=-1)

        df_fit = df2[df2[n2] < 0]
        xmin, xmax = df_fit.alpha.min(), df_fit.alpha.max()
        z0 = np.polyfit(df_fit['alpha'], df_fit[n2], 1)
        p = np.poly1d(z0)
        _nan =  df2[np.isnan(df2[n2])]['alpha'].values
        if len(_nan) > 0:
            for x0 in _nan:
                if x0 < xmin:
                    xx = np.linspace(x0,xmin,10)
                elif x0 > xmax:
                    xx = np.linspace(xmax,x0,10)
                ax.plot(xx,p(xx),c=color, ls='--', lw=1, zorder=-1)

    df_fit = df2[df2[n1] < 0]
    z0 = np.polyfit(df_fit['alpha'], df_fit[n1], 1)
    p = np.poly1d(z0)
    x1 = aopt-0.01; x2 = aopt+0.01
    xl = x1 - 0.005; xr = x1 + (x2-x1)/2
    ax.imshow([[0.,1.],[0.,1]], cmap = cmap, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=3., alpha = .6, aspect = "auto")
    xl = xr; xr = xl + (x2-x1)/2 + 0.005
    ax.imshow([[1.,0.],[1.,0]], cmap = cmap, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=3., alpha = .6, aspect = "auto")
    ax.plot(aopt, yh+0.08, c='orange', alpha=0.8, marker='v',clip_on=False)
    #ax.scatter(aopt, p(aopt), marker='o', facecolor='none', edgecolor='blue',lw=2, zorder=99)

vs_def = ['vs0_-','vs-_-']
cos1_def = ['cos0_-','cos-_-']
cos2_def = ['cos0_+','cos+_+']
cs_def = ['cs0_-', 'cs-_-']
cow1_def = ['cow0_-','cow-_-']
cow2_def = ['cow0_+','cow+_+']

amax = 0.22
yl = -6.48; yh=-3.2

plot_vbm(df_cos,0.22,ax1,annotate=False)
plot_def(df_cos,cos1_def,'C1',amax,0.060,ax1)
plot_def(df_cos,cos2_def,'C5',amax,0.069,ax1)

#ax1.set_ylabel('')
#ax1.set_yticklabels([])
#ax2.set_ylabel('')
#ax2.set_yticklabels([])

props = {"bbox":{"pad":1, "fc":"none", "ec":"none"},"fontsize":8}
#ax1.text(0.04,0.67,r'[Co$_S^-$] $\varepsilon^{HO}$($-$)',props,rotation=-21,color='k',transform=ax1.transAxes)
#ax1.text(0.50,0.70,r'[Co$_S^0$] $\varepsilon^{LU}$(0)',props,rotation=32, color='k',transform=ax1.transAxes)
#ax1.text(0.09,0.43,r'[Co$_S^0$] $\varepsilon^{HO}$(0)',props,rotation=-20,color='k',transform=ax1.transAxes)
#ax1.text(0.66,0.53,r'[Co$_S^+$] $\varepsilon^{LU}$($+$)',props,rotation=36, color='k',transform=ax1.transAxes)

ax1.text(0.065, -3, r'$\alpha_K$', ha='center')
ax1.text(0.22, -3, r'$\alpha_G$', ha='center')

ax1.text(0.23,-6.25,r'[Co$_S^0$] $\varepsilon^{HO}$(0)',props,rotation=0,color='gray')
ax1.text(0.23,-3.75,r'[Co$_S^+$] $\varepsilon^{LU}$($+$)',props,rotation=0, color='gray')

ax1.text(0.23,-5.25,r'[Co$_S^-$] $\varepsilon^{HO}$($-$)',props,rotation=0, color='k')
ax1.text(0.23,-3.20,r'[Co$_S^0$] $\varepsilon^{LU}$(0)',props,rotation=0, color='k')


for ax in [ax1]:
    ax.plot(0.22, yh+0.08, c='gray', alpha=0.8, marker='v', clip_on=False)

plt.savefig('be_cos_2.pdf', dpi=300)
