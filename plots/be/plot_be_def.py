#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 4.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Paired')
cfill = 'honeydew'
cfill = 'azure'
fig = plt.figure()
gs = fig.add_gridspec(1,1, left=0.13, right=0.89, top=0.96, bottom=0.10)
ax0 = fig.add_subplot(gs[0])

cbe = 'C0'
cvs = 'C0'
ccos1 = 'C4'
ccos2 = ccos1
ccs = 'C0'
ccow1 = 'C0'
ccow2 = ccow1

#
df_pbe0 = pd.read_csv('be_def_pbe0.dat', delim_whitespace=True, comment='#')
ax = ax0
df = df_pbe0

ax.plot(df['alpha'], df['vbm'],lw=1,ls='-',c=cbe)
ax.plot(df['alpha'], df['vbm']+df['eg'],lw=1,ls='-',c=cbe)
ax.fill_between(df['alpha'],df['vbm'],-7,color=cfill)
ax.fill_between(df['alpha'],0,df['vbm']+df['eg'],color=cfill)

df1 = df.copy() #V_s
df2 = df.drop([3]) #Co_S(-)
df3 = df.drop([3])  #Co_S(+)

#ax.scatter(df1['alpha'], df1['v_s']+df1['vbm'],marker="o",c=cvs,clip_on=False)
ax.plot(df1['alpha'], df1['v_s']+df1['vbm'],lw=0.7,ls='-',c=cvs)
#ax.scatter(df2['alpha'], df2['co_s']+df2['vbm'],marker="o",c=ccos1,clip_on=False)
ax.plot(df2['alpha'], df2['co_s']+df2['vbm'],lw=0.7,ls='-',c=ccos1)
#ax.scatter(df3['alpha'], df3['co_s+']+df3['vbm'],marker="o",c=ccos2,clip_on=False)
ax.plot(df3['alpha'], df3['co_s+']+df3['vbm'],lw=0.7,ls='-',c=ccos2)
#ax.scatter(df3['alpha'], df3['c_s']+df3['vbm'],marker="o",c=ccs,clip_on=False)
ax.plot(df3['alpha'], df3['c_s']+df3['vbm'],lw=0.7,ls='-',c=ccs)
#
ax.plot(df3['alpha'], df3['co_w']+df3['vbm'],lw=0.7,ls='-',c=ccow1)
ax.plot(df3['alpha'], df3['co_w+']+df3['vbm'],lw=0.7,ls='-',c=ccow1)

z0 = np.polyfit(df2['alpha'][1:], df2['v_s'][1:]+df2['vbm'][1:], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
aopt = 0.086
ax.scatter(aopt,p(aopt),marker="$\u25AC$",c=cvs,clip_on=False)

z0 = np.polyfit(df2['alpha'][1:], df2['co_s'][1:]+df2['vbm'][1:], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccos1, ls='-', lw=0.7, zorder=0)
aopt = 0.044
ax.scatter(aopt,p(aopt),marker="$\u25AC$",c=ccos1,clip_on=False)

z0 = np.polyfit(df3['alpha'][1:], df3['co_s+'][1:]+df3['vbm'][1:], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccos2, ls='-', lw=0.7, zorder=0)
aopt = 0.058
ax.scatter(aopt,p(aopt),marker="$\u25AC$",c=ccos2,zorder=10,clip_on=False)

z0 = np.polyfit(df3['alpha'][1:], df3['c_s'][1:]+df3['vbm'][1:], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccs, ls='-', lw=0.7, zorder=0)
aopt = 0.057
ax.scatter(aopt,p(aopt),marker="$\u25AC$",c=ccs,clip_on=False)

z0 = np.polyfit(df2['alpha'][1:], df2['co_w'][1:]+df2['vbm'][1:], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccow1, ls='-', lw=0.7, zorder=0)
aopt = 0.10
ax.scatter(aopt,p(aopt),marker="$\u25AC$",c=ccow1,clip_on=False)

z0 = np.polyfit(df2['alpha'][1:], df2['co_w+'][1:]+df2['vbm'][1:], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccow2, ls='-', lw=0.7, zorder=0)
aopt = 0.10
ax.scatter(aopt,p(aopt),marker="$\u25AC$",c=ccow2,clip_on=False)

xmax = 0.22
ax.set_xlim([0,xmax])
ax.set_ylim([-6.7,-3.0])
ax.set_ylabel(r'$E-E_{vac}$ (eV)')
ax.set_xlabel(r'$\alpha$')
ax.xaxis.set_major_locator(MultipleLocator(0.05))
ax.xaxis.set_minor_locator(MultipleLocator(0.025))
ax.yaxis.set_major_locator(MultipleLocator(0.5))
ax.yaxis.set_minor_locator(MultipleLocator(0.25))

props = {"bbox":{"pad":1, "fc":"white", "ec":"none"}}
#ax.text(0.08,0.05,'PBE0', transform=ax.transAxes)
ax.text(xmax*1.01,-3.45,r'Co$_S(0/-)$',props,rotation=0, fontsize=5)
ax.text(xmax*1.01,-4.10,r'V$_S(0/-)$',props,rotation=0, fontsize=5)
ax.text(xmax*1.01,-5.40,r'Co$_S(+/0)$',props,rotation=0, fontsize=5)
ax.text(xmax*1.01,-4.75,r'C$_S(0/-)$',props,rotation=0, fontsize=5)
ax.text(xmax*1.01,-5.90,r'Co$_W(+/0)$',props,rotation=0, fontsize=5)
ax.text(xmax*1.01,-4.40,r'Co$_W(0/-)$',props,rotation=0, fontsize=5)

#ax.annotate("",xy=(0.22,df['vbm'][3]), xycoords=ax.transData, xytext=(0.22,df['vbm'][3]+df['eg'][3]),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.2", lw=0.5, shrinkA=2, shrinkB=2, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.05"))
#ax.text(0.22+0.006,df['vbm'][3]+df['eg'][3]/2,r'Expt. band gap',props,va='center',rotation=90, fontsize=5)

plt.savefig('be_def.png', dpi=600)
plt.savefig('be_def.pdf', dpi=600)
