#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 5,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 4.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Paired')

df_cos = pd.read_csv('be_cos_pbe0.dat', delim_whitespace=True, comment='#')
df_vs = pd.read_csv('be_vs_pbe0.dat', delim_whitespace=True, comment='#')
df_cs = pd.read_csv('be_cs_pbe0.dat', delim_whitespace=True, comment='#')
df_cow = pd.read_csv('be_cow_pbe0.dat', delim_whitespace=True, comment='#')

df_cos_b = pd.read_csv('be_cos_bulk_pbe0.dat', delim_whitespace=True, comment='#')
df_cs_b = pd.read_csv('be_cs_bulk_pbe0.dat', delim_whitespace=True, comment='#')
df_vs_b = pd.read_csv('be_vs_bulk_pbe0.dat', delim_whitespace=True, comment='#')

fig = plt.figure()
gs = fig.add_gridspec(1,1, left=0.12, right=0.96, top=0.98, bottom=0.11)
ax0 = fig.add_subplot(gs[0])
cfill = 'azure'
cfill_b = 'azure'

def plot_vbm(df, xlim, ax, lc, fc, alpha=1, annotate=True, anno_text='Expt. band gap', xgap=0.22):
    ax.plot(df['alpha'], df['vbm'],lw=1,ls='-',c=lc)
    ax.plot(df['alpha'], df['vbm']+df['eg'],lw=1,ls='-',c=lc)
    ax.fill_between(df['alpha'],df['vbm'],-7,color=fc,alpha=alpha)
    ax.fill_between(df['alpha'],0,df['vbm']+df['eg'],color=fc,alpha=alpha)

    if df['alpha'].max() < xlim:
        zv = np.polyfit(df['alpha'], df['vbm'], 1)
        pv = np.poly1d(zv)
        zc = np.polyfit(df['alpha'], df['vbm']+df['eg'], 1)
        pc = np.poly1d(zc)
        xx = np.linspace(df['alpha'].max(), xlim, 10)
        ax.plot(xx, pv(xx), lw=1, ls='--', c=lc)
        ax.plot(xx, pc(xx), lw=1, ls='--', c=lc)
        ax.fill_between(xx, pv(xx), -7, color=fc, alpha=alpha)
        ax.fill_between(xx, pc(xx), 0, color=fc, alpha=alpha)

    ax.set_xlim([0,xlim])
    ax.set_ylim([-6.5,-3.0])
    ax.set_ylabel(r'$E - E_{vac}$ (eV)')
    ax.set_xlabel(r'$\alpha$')
    xtick = 0.1 if xlim > 0.4 else 0.05
    ax.xaxis.set_major_locator(MultipleLocator(xtick))
    ax.xaxis.set_minor_locator(MultipleLocator(xtick/2))
    ax.yaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_minor_locator(MultipleLocator(0.25))
    if annotate:
        props_side = {"bbox":{"pad":2, "fc":"none", "ec":"none"}}
        ax.annotate("",xy=(xgap,df['vbm'].iloc[-1]), xycoords=ax.transData, xytext=(xgap,df['vbm'].iloc[-1]+df['eg'].iloc[-1]),textcoords=ax.transData, arrowprops=dict(arrowstyle="<|-|>", color=lc, lw=0.8, shrinkA=0, shrinkB=0, patchA=None, patchB=None))
        ax.text(xgap*1.0,df['vbm'].iloc[-1]+df['eg'].iloc[-1]+0.05,r'{}'.format(anno_text), props_side,ha='center',va='bottom',rotation=0, fontsize=5)

def plot_def(df, namedef, marker, label, color, xlim, start_fit, end_fit, aopt, ax, skip_lines=False):
    cmap = cm.Oranges
    df2 = df.copy()
    n1, n2 = namedef[0], namedef[1] 
    if not skip_lines:
        ax.scatter(df2['alpha'], df2[n1], marker=".", c=color, clip_on=False, zorder=99)
        ax.plot(df2['alpha'], df2[n1],c=color,ls='-',lw=0.7)
        ax.scatter(df2['alpha'], df2[n2], marker=".", c=color, clip_on=False, zorder=99)
        ax.plot(df2['alpha'], df2[n2],c=color,ls='-',lw=0.7)

    df_fit = df2.iloc[start_fit:end_fit]
    if end_fit < len(df2):
        z0 = np.polyfit(df_fit['alpha'], df_fit[n1], 1)
        p0 = np.poly1d(z0)
        if not skip_lines:
            x0 = df_fit['alpha'].max()
            xx = np.linspace(x0,xlim,10)
            ax.plot(xx,p0(xx),c=color, ls='--', lw=0.7, zorder=-1)

        z0 = np.polyfit(df_fit['alpha'], df_fit[n2], 1)
        p1 = np.poly1d(z0)
        if not skip_lines:
            xx = np.linspace(x0,xlim,10)
            ax.plot(xx,p1(xx),c=color, ls='--', lw=0.7, zorder=-1)

        xx = np.linspace(0,xlim,1000)
        ind = np.argmin(np.abs(p0(xx) - p1(xx)))
        if not label == 'none':
            ax.scatter(xx[ind], p0(xx[ind]), marker=marker, lw=0.7, label=label, edgecolor=color, facecolor=color, zorder=10)
        else:
            ax.scatter(xx[ind], p0(xx[ind]), marker=marker, lw=0.7, edgecolor=color, facecolor=color, zorder=10)
            
    '''yl = -6.5; yh=-3.2
    x1 = aopt-0.01; x2 = aopt+0.01
    xl = x1 - 0.005; xr = x1 + (x2-x1)/2
    ax.imshow([[0.,1.],[0.,1]], cmap = cmap, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=3., alpha = .6, aspect = "auto")
    xl = xr; xr = xl + (x2-x1)/2 + 0.005
    ax.imshow([[1.,0.],[1.,0]], cmap = cmap, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=3., alpha = .6, aspect = "auto")
    '''

def plot_def_bulk(df, namedef, marker, color, ms, xlim, start_fit, end_fit, aopt, ax, skip_lines=False):
    cmap = cm.Oranges
    df2 = df.copy()
    n1, n2 = namedef[0], namedef[1] 
    if not skip_lines:
        ax.scatter(df2['alpha'], df2[n1]+df2['vbm'], marker=".", c=color, clip_on=False)
        ax.plot(df2['alpha'], df2[n1]+df2['vbm'],c=color,ls='-',lw=0.7)
        ax.scatter(df2['alpha'], df2[n2]+df2['vbm'], marker=".", c=color, clip_on=False)
        ax.plot(df2['alpha'], df2[n2]+df2['vbm'],c=color,ls='-',lw=0.7)

    df_fit = df2.iloc[start_fit:end_fit]
    if end_fit < len(df2):
        z0 = np.polyfit(df_fit['alpha'], df_fit[n1]+df_fit['vbm'], 1)
        p0 = np.poly1d(z0)
        if not skip_lines:
            x0 = df_fit['alpha'].max()
            xx = np.linspace(x0,xlim,10)
            ax.plot(xx,p0(xx),c=color, ls='--', lw=0.7, zorder=-1)

        z0 = np.polyfit(df_fit['alpha'], df_fit[n2]+df_fit['vbm'], 1)
        p1 = np.poly1d(z0)
        if not skip_lines:
            xx = np.linspace(x0,xlim,10)
            ax.plot(xx,p1(xx),c=color, ls='--', lw=0.7, zorder=-1)

        xx = np.linspace(0,xlim,1000)
        ind = np.argmin(np.abs(p0(xx) - p1(xx)))
        ax.scatter(xx[ind], p0(xx[ind]), marker=marker, s=ms, lw=0.8, edgecolor=color, facecolor='none', zorder=11)

vs_def = ['vs0_-','vs-_-']
cos1_def = ['cos0_-','cos-_-']
cos2_def = ['cos0_+','cos+_+']
cs_def = ['cs0_-', 'cs-_-']
cow1_def = ['cow0_-','cow-_-']
cow2_def = ['cow0_+','cow+_+']

amax = 0.22

plot_vbm(df_cos_b, 0.24, ax0, 'C0', cfill_b, alpha=0.4, annotate=True, anno_text=r'bulk WS$_2$',xgap=0.07)
plot_vbm(df_vs, 0.24, ax0, 'C1', cfill, alpha=1, annotate=True, anno_text=r'ML WS$_2$',xgap=0.22)
#
plot_def(df_vs,vs_def,'o', r'V$_S$', 'C4',amax,1,3,0.11,ax0,skip_lines=True)
plot_def_bulk(df_vs_b,vs_def, 'o', 'C4',16,amax,0,2,0.15,ax0,skip_lines=True)
#
#plot_vbm(df_cs,0.22,ax0,annotate=False)
plot_def(df_cs,cs_def,'s', r'C$_S$','C4',amax,1,3,0.075,ax0,skip_lines=True)
plot_def_bulk(df_cs_b,cs_def,'s', 'C4',16,amax,0,2,0.061,ax0,skip_lines=True)
#
#plot_vbm(df_cos,0.22,ax1,annotate=True)
plot_def(df_cos,cos1_def,'^', r'Co$_S$', 'C4',amax,1,3,0.06,ax0,skip_lines=True)
plot_def(df_cos,cos2_def,'^', 'none', 'C4',amax,1,3,0.07,ax0,skip_lines=True)
plot_def_bulk(df_cos_b,cos1_def,'^', 'C4',16,amax,0,2,0.0398,ax0,skip_lines=True)
#
#plot_vbm(df_cow,0.22,ax2,annotate=False)
plot_def(df_cow,cow1_def,'v',r'Co$_W$', 'C4',amax,1,3,0.10,ax0,skip_lines=True)
plot_def(df_cow,cow2_def,'v', 'none', 'C4',amax,1,3,0.10,ax0,skip_lines=True)

props = {"bbox":{"pad":1, "fc":"none", "ec":"none"},"fontsize":5}
ax0.legend(loc="lower left", ncol=2)

plt.savefig('be_w_bulk.pdf', dpi=600)
