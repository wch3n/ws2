#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Set2')

df = pd.read_csv('be_ybs.dat', delim_whitespace=True, comment='#')
fig = plt.figure()
gs = fig.add_gridspec(1,1, left=0.12, right=0.92, top=0.92, bottom=0.15, wspace=0.2)

ax = fig.add_subplot(gs[0])
cfill = 'honeydew'
cfill = 'azure'

df_be = df.drop([3])
ax.plot(df_be['alpha'], df_be['vbm'],lw=1,ls='-',c='C0')
ax.plot(df_be['alpha'], df_be['vbm']+df_be['eg'],lw=1,ls='-',c='C0')
ax.fill_between(df_be['alpha'],df_be['vbm'],-7,color=cfill)
ax.fill_between(df_be['alpha'],0,df_be['vbm']+df_be['eg'],color=cfill)

df_d = df.drop([0,1,5])
ax.set_xlim([0,0.55])
ax.set_ylim([-6.8,-3.0])
ax.set_ylabel('Energy vs vacuum (eV)')
ax.set_xlabel(r'$\alpha$')
ax.xaxis.set_major_locator(MultipleLocator(0.1))
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.yaxis.set_major_locator(MultipleLocator(0.5))
ax.yaxis.set_minor_locator(MultipleLocator(0.25))

ax.scatter(df_d['alpha'], df_d['ybs0_-']+df_d['vbm'],marker=".",c='C1',clip_on=False)
ax.plot(df_d['alpha'], df_d['ybs0_-']+df_d['vbm'],c='C1',ls='-',lw=0.7)
ax.scatter(df_d['alpha'], df_d['ybs-_-']+df_d['vbm'],marker=".",c='C1',clip_on=False)
ax.plot(df_d['alpha'], df_d['ybs-_-']+df_d['vbm'],c='C1',ls='-',lw=0.7)

props = {"bbox":{"pad":2, "fc":"none", "ec":"none"}}
props_side = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
ax.text(0.08,0.55,r'$\varepsilon(0)$',props,transform=ax.transAxes)
ax.text(0.08,0.73,r'$\varepsilon(-)$',props,transform=ax.transAxes)
#ax.text(0.8,0.67,r'$\varepsilon(0)$',props,transform=ax.transAxes)
#ax.text(0.8,0.35,r'$\varepsilon(-)$',props,transform=ax.transAxes)

ax.annotate("",xy=(0.55,df['vbm'][5]), xycoords=ax.transData, xytext=(0.55,df['vbm'][5]+df['eg'][5]),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.4", lw=0.8, shrinkA=2, shrinkB=2, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
ax.text(0.55+0.02,df['vbm'][5]+df['eg'][5]/2,r'Expt. band gap',props_side,va='center',rotation=90)

#x1 = 0.10; x2 = 0.12
#xl = x1-0.02; xr = x1 + (x2-x1)/2
#yl = -6.8; yh=-3.0
#ax.imshow([[0.,1.],[0.,1]], cmap = cm.Oranges, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .6, aspect = "auto")
#xl = xr; xr = xl + (x2-x1)/2 + 0.02
#ax.imshow([[1.,0.],[1.,0]], cmap = cm.Oranges, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .6, aspect = "auto")

plt.savefig('be_ybs.pdf')
