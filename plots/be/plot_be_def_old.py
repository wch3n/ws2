#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*1.3
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 4.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Paired')
cfill = 'honeydew'
cfill = 'azure'
fig = plt.figure()
gs = fig.add_gridspec(2,1, left=0.12, right=0.92, top=0.95, bottom=0.07, wspace=0.15)
ax0 = fig.add_subplot(gs[0])

cbe = 'C0'
cvs = 'C1'
ccos1 = 'C3'
ccos2 = 'C3'
ccs = 'C4'

df_hse = pd.read_csv('be_def_hse.dat', delim_whitespace=True, comment='#')

df = df_hse
ax = ax0
ax.plot(df['alpha'], df['vbm'],lw=1,ls='-',c=cbe)
ax.plot(df['alpha'], df['vbm']+df['eg'],lw=1,ls='-',c=cbe)
ax.fill_between(df['alpha'],df['vbm'],-7,color=cfill)
ax.fill_between(df['alpha'],0,df['vbm']+df['eg'],color=cfill)

df1 = df.copy() #V_s
df2 = df.drop([3,4]) #Co_S(-)
df3 = df.drop([4])  #Co_S(+)

ax.scatter(df1['alpha'], df1['v_s']+df1['vbm'],marker="$\u25AC$",c=cvs,clip_on=False)
ax.plot(df1['alpha'], df1['v_s']+df1['vbm'],lw=0.7,ls='-',c=cvs)
ax.scatter(df2['alpha'], df2['co_s']+df2['vbm'],marker="$\u25AC$",c=ccos1,clip_on=False)
ax.plot(df2['alpha'], df2['co_s']+df2['vbm'],lw=0.7,ls='-',c=ccos1)
ax.scatter(df3['alpha'], df3['co_s+']+df3['vbm'],marker="$\u25AC$",c=ccos2,clip_on=False)
ax.plot(df3['alpha'], df3['co_s+']+df3['vbm'],lw=0.7,ls='-',c=ccos2)

z0 = np.polyfit(df2['alpha'], df2['co_s']+df2['vbm'], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.55,10)
ax.plot(xx,p(xx),c=ccos1, ls='--', lw=0.7, zorder=10)

z0 = np.polyfit(df3['alpha'], df3['co_s+']+df3['vbm'], 1)
p = np.poly1d(z0)
xx = np.linspace(0.25,0.55,10)
ax.plot(xx,p(xx),c=ccos2, ls='--', lw=0.7, zorder=10)

ax.set_xlim([0,0.55])
ax.set_ylim([-6.8,-3.0])
ax.set_ylabel(r'$E-E_{vac}$ (eV)')
ax.set_xlabel(r'$\alpha$')

ax.xaxis.set_major_locator(MultipleLocator(0.1))
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.yaxis.set_major_locator(MultipleLocator(0.5))
ax.yaxis.set_minor_locator(MultipleLocator(0.25))

ax.text(0.20,0.65,r'[V$_S$] $\mu(0/-)$',transform=ax.transAxes)
ax.text(0.20,0.88,r'[Co$_S$] $\mu(0/-)$',transform=ax.transAxes)
ax.text(0.20,0.28,r'[Co$_S$] $\mu(+/0)$',transform=ax.transAxes)
ax.text(0.05,0.05,'HSE', transform=ax.transAxes)

props = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
ax.annotate("",xy=(0.55,df['vbm'][4]), xycoords=ax.transData, xytext=(0.55,df['vbm'][4]+df['eg'][4]),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.4", lw=0.8, shrinkA=2, shrinkB=2, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
ax.text(0.55+0.02,df['vbm'][4]+df['eg'][4]/2,r'Expt. band gap',props,va='center',rotation=90)

#
df_pbe0 = pd.read_csv('be_def_pbe0.dat', delim_whitespace=True, comment='#')
ax1 = fig.add_subplot(gs[1])
ax = ax1
df = df_pbe0

ax.plot(df['alpha'], df['vbm'],lw=1,ls='-',c=cbe)
ax.plot(df['alpha'], df['vbm']+df['eg'],lw=1,ls='-',c=cbe)
ax.fill_between(df['alpha'],df['vbm'],-7,color=cfill)
ax.fill_between(df['alpha'],0,df['vbm']+df['eg'],color=cfill)

df1 = df.copy() #V_s
df2 = df.drop([3]) #Co_S(-)
df3 = df.drop([3])  #Co_S(+)

ax.scatter(df1['alpha'], df1['v_s']+df1['vbm'],marker="$\u25AC$",c=cvs,clip_on=False)
ax.plot(df1['alpha'], df1['v_s']+df1['vbm'],lw=0.7,ls='-',c=cvs)
ax.scatter(df2['alpha'], df2['co_s']+df2['vbm'],marker="$\u25AC$",c=ccos1,clip_on=False)
ax.plot(df2['alpha'], df2['co_s']+df2['vbm'],lw=0.7,ls='-',c=ccos1)
ax.scatter(df3['alpha'], df3['co_s+']+df3['vbm'],marker="$\u25AC$",c=ccos2,clip_on=False)
ax.plot(df3['alpha'], df3['co_s+']+df3['vbm'],lw=0.7,ls='-',c=ccos2)
ax.scatter(df3['alpha'], df3['c_s']+df3['vbm'],marker="$\u25AC$",c=ccs,clip_on=False)
ax.plot(df3['alpha'], df3['c_s']+df3['vbm'],lw=0.7,ls='-',c=ccs)

z0 = np.polyfit(df2['alpha'], df2['co_s']+df2['vbm'], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccos1, ls='--', lw=0.7, zorder=10)

z0 = np.polyfit(df3['alpha'], df3['co_s+']+df3['vbm'], 1)
p = np.poly1d(z0)
xx = np.linspace(0.10,0.22,10)
ax.plot(xx,p(xx),c=ccos2, ls='--', lw=0.7, zorder=10)

ax.set_xlim([0,0.22])
ax.set_ylim([-6.8,-3.0])
ax.set_ylabel(r'$E-E_{vac}$ (eV)')
ax.set_xlabel(r'$\alpha$')
ax.xaxis.set_major_locator(MultipleLocator(0.1))
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.yaxis.set_major_locator(MultipleLocator(0.5))
ax.yaxis.set_minor_locator(MultipleLocator(0.25))

ax.text(0.05,0.05,'PBE0', transform=ax.transAxes)

props = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
ax.annotate("",xy=(0.22,df['vbm'][3]), xycoords=ax.transData, xytext=(0.22,df['vbm'][3]+df['eg'][3]),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.4", lw=0.8, shrinkA=2, shrinkB=2, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
ax.text(0.22+0.008,df['vbm'][3]+df['eg'][3]/2,r'Expt. band gap',props,va='center',rotation=90)

plt.savefig('be_def.png', dpi=600)
