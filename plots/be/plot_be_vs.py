#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*1.6
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Set2')

df_hse = pd.read_csv('be_vs_hse.dat', delim_whitespace=True, comment='#')
df_pbe0 = pd.read_csv('be_vs_pbe0.dat', delim_whitespace=True, comment='#')
fig = plt.figure()
gs = fig.add_gridspec(2,1, left=0.12, right=0.92, top=0.98, bottom=0.08, hspace=0.2)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])
cfill = 'azure'

def plot_vbm(df,xlim,ax):
    ax.plot(df['alpha'], df['vbm'],lw=1,ls='-',c='C0')
    ax.plot(df['alpha'], df['vbm']+df['eg'],lw=1,ls='-',c='C0')
    ax.fill_between(df['alpha'],df['vbm'],-7,color=cfill)
    ax.fill_between(df['alpha'],0,df['vbm']+df['eg'],color=cfill)

    ax.set_xlim([0,xlim])
    ax.set_ylim([-6.8,-3.0])
    ax.set_ylabel(r'$E - E_{vac}$ (eV)')
    ax.set_xlabel(r'$\alpha$')
    xtick = 0.1 if xlim > 0.4 else 0.05
    ax.xaxis.set_major_locator(MultipleLocator(xtick))
    ax.xaxis.set_minor_locator(MultipleLocator(xtick/2))
    ax.yaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_minor_locator(MultipleLocator(0.25))
    props_side = {"bbox":{"pad":2, "fc":"white", "ec":"none"}}
    ax.annotate("",xy=(xlim,df['vbm'].iloc[-1]), xycoords=ax.transData, xytext=(xlim,df['vbm'].iloc[-1]+df['eg'].iloc[-1]),textcoords=ax.transData, arrowprops=dict(arrowstyle="-", color="0.4", lw=0.5, shrinkA=2, shrinkB=2, patchA=None, patchB=None, connectionstyle="bar, fraction=-0.08"))
    ax.text(xlim*1.03,df['vbm'].iloc[-1]+df['eg'].iloc[-1]/2,r'Expt. band gap',props_side,va='center',rotation=90)

def plot_def(df,xlim,x0,start_fit,end_fit,box_x0,box_x1,ax):
    df2 = df.copy()

    ax.scatter(df2['alpha'], df2['vs0_-']+df2['vbm'],marker=".",c='C1',clip_on=False)
    ax.plot(df2['alpha'], df2['vs0_-']+df2['vbm'],c='C1',ls='-',lw=0.7)
    ax.scatter(df2['alpha'], df2['vs-_-']+df2['vbm'],marker=".",c='C1',clip_on=False)
    ax.plot(df2['alpha'], df2['vs-_-']+df2['vbm'],c='C1',ls='-',lw=0.7)

    '''df_fit = df2.iloc[start_fit:end_fit]
    z0 = np.polyfit(df_fit['alpha'], df_fit['vs0_-']+df_fit['vbm'], 1)
    p = np.poly1d(z0)
    xx = np.linspace(x0,xlim,10)
    ax.plot(xx,p(xx),c='C1', ls='--', lw=0.7, zorder=-1)

    z0 = np.polyfit(df_fit['alpha'], df_fit['vs-_-']+df_fit['vbm'], 1)
    p = np.poly1d(z0)
    xx = np.linspace(x0,xlim,10)
    ax.plot(xx,p(xx),c='C1', ls='--', lw=0.7, zorder=-1)'''

    x1 = box_x0; x2 = box_x1
    xl = x1 - 0.02; xr = x1 + (x2-x1)/2
    yl = -6.8; yh=-3.0
    ax.imshow([[0.,1.],[0.,1]], cmap = cm.Oranges, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .6, aspect = "auto")
    xl = xr; xr = xl + (x2-x1)/2 + 0.02
    ax.imshow([[1.,0.],[1.,0]], cmap = cm.Oranges, extent = [xl, xr, yl, yh], interpolation = "bilinear", vmin=0, vmax=2.5, alpha = .6, aspect = "auto")

plot_vbm(df_hse,0.55,ax0)
plot_def(df_hse,0.55,0.05,1,3,0.167,0.177,ax0)
plot_vbm(df_pbe0,0.22,ax1)
plot_def(df_pbe0,0.22,0.05,1,3,0.08,0.09,ax1)

props = {"bbox":{"pad":1, "fc":"none", "ec":"none"}}
ax0.text(0.40,0.70,r'$\varepsilon(0)$',props,color='C1',transform=ax0.transAxes)
ax0.text(0.40,0.52,r'$\varepsilon(-)$',props,color='C1',transform=ax0.transAxes)

plt.savefig('be_vs.pdf')
