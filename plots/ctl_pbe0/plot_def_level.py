#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches

def parse(params,mu,X,dvbm):
    params = np.genfromtxt(params)
    n_plt = params.shape[1] - 1
    eform_dict = {}
    ctl_dict = {}
    params[:,1] += params[:,0]*dvbm
    print(params)
    for il in range(n_plt):
        eform = np.zeros((len(params), 101))
        for i in range(len(params)):
            eform[i] = list(map(lambda x: (x-X[0])*params[i,0] + params[i,il+1], X))
        eform_min = np.amin(eform, axis=0) + mu
        #
        _c = [params[i,il+1] - params[i+1,il+1] + X[0] for i in range(len(params)-1)]
        _ctl = []
        i = 0
        while i < len(_c):
            if (i == len(_c)-1) or (_c[i] > _c[i+1]):
                _ctl.append(_c[i])
                i += 1
            else:
                _ctl.append(0.5*(_c[i]+_c[i+1]))
                i += 2
        ctl = np.zeros((len(_ctl),2))
        for i,v in enumerate(_ctl):
            ctl[i,0] = v
            ctl[i,1] = eform_min[np.argmin(np.abs(X-v))]
        eform_dict[il] = eform_min
        ctl_dict[il] = ctl
    return eform_dict, ctl_dict

def plot_ctl(x, eform_dict, ctl_dict, color, ax, zorder=9):
    for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], c=color, lw=1.2, solid_capstyle='round', zorder=zorder)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], c=color)

fig_width = 8.5/2.54
fig_height = fig_width*0.75
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 7,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Paired')

vbm_22 = -6.328
vbm_05 = -5.775

gap_22 = 2.908
gap_05 = 2.192

cbm_22 = vbm_22 + gap_22
cbm_05 = vbm_05 + gap_05

dvb_05 = vbm_22 - vbm_05

yo = 0; ye=4.5
fig = plt.figure()
gs = fig.add_gridspec(1,1, left=0.1, right=0.92, top=0.96, bottom=0.12)
ax0 = fig.add_subplot(gs[0])
x_22 = np.linspace(0,gap_22,101)
x_05 = np.linspace(0,gap_05,101)
patches = []

kwargs = {"fontsize":6}

mu1 = {"S": 0, "Co": 0}
mu2 = {"S": 0, "Co": 0}

eform_v_s, ctl_v_s = parse('v_s.params', mu1['S'], x_22, dvb_05)
eform_co_s, ctl_co_s = parse('co_s.params', mu2['S']-mu2['Co']/2, x_22, dvb_05)
eform_yb_s, ctl_yb_s = parse('yb_s.params', 0, x_22, dvb_05)
eform_co_w, ctl_co_w = parse('co_w.params', 0, x_22, dvb_05)
plot_ctl(x_22, eform_v_s, ctl_v_s, 'C0', ax0, zorder=1)
plot_ctl(x_22, eform_co_s, ctl_co_s, 'C4', ax0, zorder=1)
plot_ctl(x_22, eform_yb_s, ctl_yb_s, 'C8', ax0, zorder=1)
plot_ctl(x_22, eform_co_w, ctl_co_w, 'C6', ax0, zorder=1)
ax0.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(0,gap_22,yo,ye), alpha=0.1, aspect='auto')

ax0.set_xlabel('Fermi level (eV)')
ax0.set_ylabel('Formation energy (eV)')
ax0.xaxis.set_minor_locator(MultipleLocator(0.2))
ax0.xaxis.set_major_locator(MultipleLocator(0.4))
ax0.yaxis.set_major_locator(MultipleLocator(1))
ax0.yaxis.set_minor_locator(MultipleLocator(0.5))

#ax0.text(0.23,0.88, r'$(+/0)$', transform=ax0.transAxes)
#ax0.text(0.72,0.88, r'$(0/-)$', transform=ax0.transAxes)
#ax0.text(0.80,0.48, r'$(0/-)$', transform=ax0.transAxes)
#ax0.text(0.78,0.30, r'$(0/-)$', transform=ax0.transAxes)
ax0.text(2.95,0.25, r'Yb$_S$')
ax0.text(2.95,0.96, r'V$_S$')
ax0.text(2.95,1.96, r'Co$_S$')
ax0.text(2.95,3.06, r'Co$_W$')

plt.savefig('def_level.pdf')
