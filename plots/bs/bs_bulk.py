#!/usr/bin/env python3

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
import pandas as pd

fig_width = 8.5/2.54
fig_height = fig_width*1.2
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('Set2')
fig = plt.figure()
gs = fig.add_gridspec(2,2, left=0.12, right=0.98, top=0.95, bottom=0.08, wspace=0.25)
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1])
ax2 = fig.add_subplot(gs[2])
ax3 = fig.add_subplot(gs[3])

df_hse = pd.read_csv('hse_15_bulk.dat', delim_whitespace=True, header=None)
df_pbe0 = pd.read_csv('pbe0_7_bulk.dat', delim_whitespace=True, header=None)

df_be_hse = pd.read_csv('be_hse_bulk.dat', delim_whitespace=True)
df_be_pbe0 = pd.read_csv('be_pbe0_bulk.dat', delim_whitespace=True)

ticklabels = [r'${\Gamma}$', r'${M}$', r'${K}$', r'${\Gamma}$']
ticks = [0, 1.15162, 1.81651, 3.14628]
vbm_hse =  7.27
vbm_pbe0 = 7.30
offset_gw = -6.178915
ngrid = 274

vbm_gw = -6.20

def draw_bs(df,nb,offset,label,ax):
    bs = df.values.reshape([nb,ngrid,2])
    #bs_gw = df_g0w0.values.reshape([32,ngrid,2])
    for i in bs:
        ax.plot(i[:,0],i[:,1]-offset,ls='-',lw=0.6,color='C2', zorder=1)
    #for i in bs_gw:
    #    ax.plot(i[:,0],i[:,1]-offset_gw,ls='--',lw=0.6,color='C3', zorder=1)
    ax.set_ylim([-8,6])
    ax.set_xlim(ticks[0], ticks[-1])
    ax.set_xticklabels(ticklabels)
    ax.set_xticks(ticks)
    ax.xaxis.set_ticks_position('none')
    ax.axvline(ticks[1],ls='--',lw=0.5,color='gray',zorder=-1)
    ax.axvline(ticks[2],ls='--',lw=0.5,color='gray', zorder=-1)
    ax.yaxis.set_major_locator(MultipleLocator(2))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    ax.text(0.5,1.02, label, ha='center', transform=ax.transAxes)

def draw_be(df,c,gap_point,label,ax):
    df = df[['alpha','VBM','Eg']].dropna()
    df['VBM'] -= df.iloc[0]['VBM']
    ax.plot(df['alpha'], df['VBM'], '-',lw=0.6,color=c)
    ax.plot(df['alpha'], df['VBM']+df['Eg'],'-',lw=0.6,color=c)
    ax.scatter(gap_point,df[df['alpha'] == gap_point]['VBM'], color=c)
    ax.scatter(gap_point,df[df['alpha'] == gap_point]['VBM']+df[df['alpha'] == gap_point]['Eg'], color=c)
    ax.fill_between(df['alpha'], df['VBM'], -3.0, color=c, alpha=0.1)
    ax.fill_between(df['alpha'], df['VBM']+df['Eg'], 2, color=c, alpha=0.1)
    #ax.axhline(vbm_gw, lw=0.6, ls='--', color='C3')
    #ax.axhline(vbm_gw+2.90, lw=0.6, ls='--', color='C3')
    ax.set_xlim([0,25])
    ax.set_ylim([-1.,2.])
    ax.set_xlabel(r'$\alpha$ (%)')
    ax.yaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_minor_locator(MultipleLocator(0.25))
    ax.text(0.2,0.6, label, ha='center', transform=ax.transAxes)

draw_bs(df_hse,40,vbm_hse,r'HSE ($\alpha=0.15, \omega=0.106\,\AA^{-1}$)', ax0)
draw_bs(df_pbe0,40,vbm_pbe0,r'PBE0 ($\alpha=0.07$)',ax1)
ax0.set_ylabel(r'$E - E_{VBM}$ (eV)')
ax2.set_ylabel(r'$E - E_{VBM}^{PBE}$ (eV)')
ax2.yaxis.set_label_coords(-0.17,0.5)

draw_be(df_be_hse,'C2',15,'HSE',ax2)
draw_be(df_be_pbe0,'C2',7,'PBE0',ax3)

plt.savefig('bs_bulk.pdf')
