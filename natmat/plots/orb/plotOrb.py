#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as patches
from ase.io.vasp import read_vasp
from ase import Atoms
from ase.visualize import plot
from os.path import join
from pymatgen.electronic_structure.core import Orbital, OrbitalType, Spin
from pymatgen.io.vasp import Vasprun
from math import sqrt, pi

def get_pdos(xml):
    sigma = 0.04
    r = Vasprun(xml)
    dos = r.complete_dos
    orbs = {Orbital.dxz:r'd$_{xz}$', Orbital.dxy:r'd$_{xy}$', Orbital.dyz:r'd$_{yz}$', Orbital.dz2:r'd$_{z^2}$', Orbital.dx2:r'd$_{x^2-y^2}$'}
    energies = dos.energies
    pdos_res = {}
    tdos_res = {}
    pdos_res[Spin.up] = {}
    if r.is_spin:
        pdos_res[Spin.down] = {}
    
    for i in orbs:
        densities = dos.get_site_orbital_dos(r.final_structure[-1], i).densities
        pdos = np.c_[energies, densities[Spin.up]]
        _, pdos_smeared = smear(pdos, sigma)
        pdos_res[Spin.up][i] = pdos_smeared
        if r.is_spin: 
            pdos = np.c_[energies, densities[Spin.down]]
            _, pdos_smeared = smear(pdos,sigma)
            pdos_res[Spin.down][i] = pdos_smeared

    co_dos = r.complete_dos.get_site_dos(r.final_structure[-1]).densities
    _, dos_smeared = smear(np.c_[energies, dos.densities[Spin.up]-co_dos[Spin.up]],sigma)
    tdos_res[Spin.up] = dos_smeared

    if r.is_spin: 
        _, dos_smeared = smear(np.c_[energies, dos.densities[Spin.down]-co_dos[Spin.down]],sigma)
        tdos_res[Spin.down] = dos_smeared
    return energies, pdos_res, tdos_res, r.is_spin

def smear(pdos, sigma):
    gauss = lambda x, sigma: 1.0/sqrt(2*pi)/sigma*np.exp(-0.5*x**2/sigma**2)
    dx = pdos[1,0] - pdos[0,0]
    g = gauss(np.arange(-1,1,dx), sigma)*dx
    return pdos[:,0], np.convolve(g, pdos[:,1], 'same')

def connect(x1, x2, y1, y2, ax, shrinkA=1, shrinkB=1, armA=-10, armB=10):
    connectionstyle = "arc,angleA=0,angleB=0,armA={0},armB={1},rad=0".format(armA, armB)
    props = dict(arrowstyle="-", color="gray", lw = 0.6, ls = '--', shrinkA=shrinkA, shrinkB=shrinkB, patchA=None, patchB=None, connectionstyle=connectionstyle)
    ax.annotate('', xy=(x1,y1), xycoords='data', xytext=(x2,y2), textcoords='data', arrowprops = props, horizontalalignment='left')

def plot_cow3(poscar,dy,ax,offsetx=-5.5):
    s = read_vasp(poscar)
    co = s[-1]; w1 = s[9]; w2 = s[10]; w3 = s[15]
    co.position += [0,dy,0]
    cow3 = Atoms('CoW3', positions=(co.position,w1.position,w2.position,w3.position))
    plot.plot_atoms(cow3, offset=[offsetx,-7.5,0], radii = [0.85,0.8,0.8,0.8], colors=['cornflowerblue','lightgray','lightgray','lightgray'], ax=ax)
    ax.axis('off')

fig_width = 10/2.54
fig_height = fig_width*0.7
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'normal',
          'font.size': 7,
          'font.weight': 'normal',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

mc = 'k'
props = dict(arrowstyle= '->', color=mc, lw=0.8, ls='-')
w0=0.25
bbox = dict(ec='none',fc='none',pad=1)
kwargs = dict(fontsize=6)
barwidth = 0.06
colorDos = {Orbital.dxz:'C3', Orbital.dxy:'C1', Orbital.dyz:'C2', Orbital.dz2:'C0', Orbital.dx2:'C4'}
textDos = {Orbital.dxz:r'$d_{xz}$', Orbital.dxy:r'$d_{xy}$', Orbital.dyz:r'$d_{yz}$', Orbital.dz2:r'$d_{z^2}$', Orbital.dx2:r'$d_{x^2-y^2}$'}

def draw_diagram(workdir, q, l0, vac, ax, legend=False, lg_posx=0.915):
    df = pd.read_csv(join(workdir,q, 'orb.dat'))
    energies, pdos, tdos, is_spin = get_pdos(join(workdir,q,'vasprun.xml.gz'))
    energies -= vac
    if is_spin:
        x0 = l0+barwidth+0.015
        for i in pdos[Spin.up]:
            ax.fill_betweenx(energies, -pdos[Spin.up][i]/100+x0, x0, color=colorDos[i], lw=0.1, alpha=0.6, label=textDos[i])
            ax.fill_betweenx(energies, -tdos[Spin.up]/500+x0, x0, facecolor='lightgray', edgecolor='none', lw=0.5, alpha=0.2, zorder=0)
        for i in pdos[Spin.down]:
            ax.fill_betweenx(energies, pdos[Spin.down][i]/100+x0, x0, color=colorDos[i], lw=0.1, alpha=0.6)
            ax.fill_betweenx(energies, tdos[Spin.down]/500+x0, x0, facecolor='lightgray', edgecolor='none',lw=0.5, alpha=0.2, zorder=0)
    else:
        x0 = l0+barwidth+0.10
        for i in pdos[Spin.up]:
            ax.fill_betweenx(energies, -pdos[Spin.up][i]/100+x0, x0, color=colorDos[i], lw=0.1, alpha=0.6)
            ax.fill_betweenx(energies, -tdos[Spin.up]/500+x0, x0, facecolor='lightgray', edgecolor='none', lw=0.5, alpha=0.2)
    
    vbm_energy = df[(df.belike == 'VBM')]['energy'].max()
    cbm_energy = df[(df.belike == 'CBM')]['energy'].min()
    for i, r in df.iterrows():
        w = barwidth
        if r['occ'] == 0:
            if r['energy'] >= cbm_energy:
                continue
            if r['spin'] == 'up':
                l = l0
            elif r['spin'] in ['dn', 'down']:
                l = l0 + w + 0.03
            else:
                w += 0.0; l = l0+0.06
            h = 0.02
            rect = patches.Rectangle((l,r['energy']-h/2), w, h, linewidth=0.3, edgecolor=mc, facecolor='none')
            ax.add_patch(rect)
        elif r['occ'] > 0:
            if r['energy'] <= vbm_energy:
                continue
            if r['spin'] == 'up':
                l = l0
            elif r['spin'] in ['dn', 'down']:
                l = l0 + w + 0.03
            else:
                w += 0.0; l = l0+0.06
            h = 0.01
            if r['band'] > 0:
                rect = patches.Rectangle((l,r['energy']-h/2), w, h, linewidth=0.5, edgecolor=mc, facecolor=mc)
                ax.add_patch(rect)
            else:
                h = 0.03
                rect = patches.Rectangle((l,r['energy']-h/2), w, h, linewidth=0.3, edgecolor=mc, facecolor='k', ls='--', alpha=0.3)
                ax.add_patch(rect)
            if r['occ'] == 1 and r['spin'] == 'up':
                ax.arrow(l+w/2,r['energy']-0.05, 0,  0.12, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
            if r['occ'] == 1 and r['spin'] in ['dn', 'down']:
                ax.arrow(l+w/2,r['energy']+0.05, 0, -0.12, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
            if r['occ'] == 1 and r['spin'] == 'both':
                ax.arrow(l+w/2-0.005,r['energy']-0.05, 0, 0.14, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
                ax.arrow(l+w/2+0.005,r['energy']+0.06, 0, -0.14, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
            if r['occ'] == 4 and r['spin'] == 'both':
                ax.arrow(l+w/2-0.02,r['energy']-0.05, 0, 0.14, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
                ax.arrow(l+w/2-0.01,r['energy']+0.06, 0, -0.14, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
                ax.arrow(l+w/2+0.01,r['energy']-0.05, 0, 0.14, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)
                ax.arrow(l+w/2+0.02,r['energy']+0.06, 0, -0.14, color=mc, shape='right', head_length=0.05, head_width=0.01,length_includes_head=True, overhang=0.2, lw=0.4)

        if r['label'] != 0 and r['band'] != -1:
            ax.text(l+w/2-0.025,r['energy']+r['label']*0.15,r'${0}$'.format(r['char']),bbox=bbox, zorder=99, **kwargs)
        elif r['band'] == -1:
            if r['label'] == 0:
                ax.text(l+w/2+0.05,r['energy']-0.03,r'${0}$'.format(r['char']),bbox=bbox, zorder=99, **kwargs)
            else:
                ax.text(l+w/2-0.025,r['energy']+r['label']*0.15,r'${0}$'.format(r['char']),bbox=bbox, zorder=99, **kwargs)
   
    vbm = -6.06; cbm = -3.48    
 
    ax.axhline(vbm, xmax=0.91, lw=0.5, ls="--", color='C8', zorder=-1)
    ax.axhline(cbm, xmax=0.91, lw=0.5, ls="--", color='C8', zorder=-1)

    if legend:
        lg = ax.legend(bbox_to_anchor=(lg_posx,0.8),handlelength=1)
        lg.get_frame().set_alpha(None)
    props = dict(facecolor='w', edgecolor='none', pad=0.01)
    ax.text(0.81, vbm-0.01, r'$\blacktriangleleft$', color='C8', fontsize=4, va='center') 
    ax.text(0.83, vbm-0.01, r'$\varepsilon_{VBM}$', color='C8', fontsize=6, va='center', bbox=props) 
    ax.text(0.81, cbm-0.01, r'$\blacktriangleleft$', color='C8', fontsize=4, va='center') 
    ax.text(0.83, cbm-0.01, r'$\varepsilon_{CBM}$', color='C8', fontsize=6, va='center', bbox=props) 
    return df

def run(workdir='.',fig=None,gsf=None,separate_draw=False, lg_posx=0.915):
    sns.set_palette('Set1')
    if fig == None:
        fig = plt.figure()
        gs = fig.add_gridspec(ncols=1,nrows=2, height_ratios=[1,6], left=0.11, right=0.94, top=1.00, bottom=0.02, hspace=0)
    else:
        gs = gsf.subgridspec(ncols=1,nrows=2,height_ratios=[1,6],hspace=0) 
    ax = fig.add_subplot(gs[1])
    upper_grid = gs[0].subgridspec(ncols=3, nrows=1)
    ax0 = fig.add_subplot(upper_grid[0]) 
    ax1 = fig.add_subplot(upper_grid[1]) 
    ax2 = fig.add_subplot(upper_grid[2]) 

    dfp = draw_diagram(workdir, '1+', 0.05, 3.3, ax, legend=True, lg_posx=lg_posx)
    dfo = draw_diagram(workdir, '0', 0.35, 3.2, ax)
    dfn = draw_diagram(workdir, '1-', 0.65, 3.1, ax)
    
    ax.text(-0.75,0.7,r'$Co_S^{+1}$ $(C_s) $',transform=ax0.transAxes, fontsize=6)
    ax.text(-0.68,0.7,r'$Co_S^{0}$ $(C_s)$', transform=ax1.transAxes, fontsize=6)
    ax.text(0.10,0.7,r'$Co_S^{-1}$ $(C_{3v})$', transform=ax2.transAxes, fontsize=6)
    ax.set_xlim([0.03,0.88])
    ax.set_ylim([-6.4,-3.2])
    ax.set_ylabel('Energy (eV)')

    plot_cow3(join(workdir,'1+','POSCAR'), -0.5, ax0)
    plot_cow3(join(workdir,'0','POSCAR'),  -0.3, ax1)
    plot_cow3(join(workdir,'1-','POSCAR'), 0., ax2, offsetx=0.2)

    connect(0.55, 0.69, dfo.iloc[-5]['energy'], dfn.iloc[-6]['energy'],ax)
    #connect(0.57, 0.68, dfo.iloc[-4-2]['energy'], dfn.iloc[-4-2]['energy'],ax)
    #connect(0.57, 0.68, dfo.iloc[-3-2]['energy'], dfn.iloc[-2-2]['energy'],ax)

    connect(0.27, 0.42, dfp.iloc[-5]['energy'], dfo.iloc[-6]['energy'], ax, armA=-15, armB=10)
    #connect(0.27, 0.46, dfp.iloc[-4-2]['energy'], dfo.iloc[-4-2]['energy'], ax, armA=-15, armB=10)
    #connect(0.27, 0.46, dfp.iloc[-2-2]['energy'], dfo.iloc[-2-2]['energy'], ax, armA=-15, armB=10)

    ax.get_xaxis().set_visible(False)
    if separate_draw:
        plt.savefig('orb.pdf')
    ax.yaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_minor_locator(MultipleLocator(0.25))

    return ax

if __name__ == '__main__':
    run(separate_draw=True)
