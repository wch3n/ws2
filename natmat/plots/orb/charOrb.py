#!/usr/bin/env python3

from pymatgen.io.vasp import Vasprun, Procar, Wavecar
from pymatgen.electronic_structure.core import Spin
import numpy as np

def get_wfr(wavcar,spin,kpt,band):
    return np.fft.ifftn(wavcar.fft_mesh(kpt, band, spin))*ngvec

def be_like(spin, ib, occ):
    proj = p.data[spin][0,ib,:-1].sum(axis=0)
    if occ > 0:
        result = 'VBM' if (proj[4] > 0.1) and (proj[-1] > 0.1) else ''
    else:
        result = 'CBM' if proj[6] > 0.5 else ''
    return result

def orbs(spin, spin_name):
    nb_occ = np.argmin(r.eigenvalues[spin][0][:,1] > 0)
    for ib in range(nb_occ-5, nb_occ+5):
        eigenvalue, std = r.eigenvalues[spin][:,ib].mean(axis=0), r.eigenvalues[spin][:,ib].std(axis=0)[0]
        proj = p.data[spin][:,ib,-1].mean(axis=0)
        orbs_strong = [p.orbitals[i] for i in np.where(proj > 0.5)[0]]
        orbs_weak = [p.orbitals[i] for i in np.where((proj < 0.5) & (proj >0.03))[0]]
        occ = eigenvalue[1]
        print('{0:},{1:},{2:},{3:},{4:},'.format(spin_name, ib, '+'.join(orbs_strong), '+'.join(orbs_weak), be_like(spin, ib, occ))+'{0:.2f},{2:.0f},0.0'.format(eigenvalue[0] - vac, std, occ))

r = Vasprun('vasprun.xml')
p = Procar('PROCAR')
vac = 3.1

print('spin,band,char,char_weak,belike,energy,occ,label')
if r.is_spin:
    for spin in [Spin.up, Spin.down]:
        orbs(spin, spin.name)
else:
    orbs(Spin.up, 'both')
