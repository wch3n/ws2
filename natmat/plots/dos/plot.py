#!/usr/bin/env python3

from pymatgen.core import Element
from pymatgen.electronic_structure.core import Orbital, OrbitalType, Spin
from pymatgen.io.vasp import Vasprun
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
from math import sqrt, pi

def smear(pdos, sigma):
    gauss = lambda x, sigma: 1.0/sqrt(2*pi)/sigma*np.exp(-0.5*x**2/sigma**2)
    dx = pdos[1,0] - pdos[0,0]
    g = gauss(np.arange(-1,1,dx), sigma)*dx
    return pdos[:,0], np.convolve(g, pdos[:,1], 'same')

fig_width = 8.8/2.54
fig_height = fig_width*0.75
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 8,
          'axes.labelweight': 'light',
          'font.size': 8,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 7,
          'ytick.labelsize': 7,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
sns.set_palette('tab10')

#r = Vasprun('vasprun.xml.gz')
r = Vasprun('vasprun_dftu.xml.gz')
dos = r.complete_dos

orbs = {Orbital.dxz:r'd$_{xz}$', Orbital.dxy:r'd$_{xy}$', Orbital.dyz:r'd$_{yz}$', Orbital.dz2:r'd$_{z^2}$', Orbital.dx2:r'd$_{x^2-y^2}$'}
colors = {Orbital.dxz:'C0', Orbital.dxy:'C1', Orbital.dyz:'C2', Orbital.dz2:'C3', Orbital.dx2:'C4'}

energies = dos.energies

for i in orbs:
    densities = dos.get_site_orbital_dos(r.final_structure[-1], i).densities
    pdos = np.c_[energies, densities[Spin.up]]
    energies, pdos_smeared = smear(pdos,0.05)
    plt.plot(energies, pdos_smeared, label=orbs[i], color=colors[i], lw=0.5)
    pdos = np.c_[energies, -densities[Spin.down]]
    energies, pdos_smeared = smear(pdos,0.05)
    plt.plot(energies, pdos_smeared, color=colors[i], lw=0.5)

energies, dos_smeared = smear(np.c_[energies, dos.densities[Spin.up]],0.05)
plt.fill_between(energies, dos_smeared, color='lightgray')

energies, dos_smeared = smear(np.c_[energies, dos.densities[Spin.down]],0.05)
plt.fill_between(energies, -dos_smeared, color='lightgray')

plt.xlim([-6,1])
plt.ylim([-4,4])
plt.legend()

plt.savefig('dos_dftu.pdf')
#plt.savefig('dos.pdf')
