#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator, NullLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches
from PIL import Image

def plot_png(filename, ax):
    img = np.asarray(Image.open(filename))
    h, w = img.shape[0], img.shape[1]
    ax.imshow(img[:,int(w/2-500):int(w/2+500),:])
    ax.xaxis.set_major_locator(NullLocator())
    ax.yaxis.set_major_locator(NullLocator())

def draw_coords(ax):
    props = dict(arrowstyle='<-', lw=0.8, ls='-')
    ax.arrow(0.06,0.04,0.2,0.0, head_width=0.02, head_length=0.05, lw=0.5, fc='k', ec='k', transform=ax.transAxes)
    ax.arrow(0.06,0.04,0.,0.13, head_width=0.03, head_length=0.04, lw=0.5, fc='k', ec='k', transform=ax.transAxes)
    ax.text(0.25,0.06,'x', fontsize=6, transform=ax.transAxes)
    ax.text(0.10,0.18,'y', fontsize=6, transform=ax.transAxes)
    ax.plot(0.06,0.04,".", ms=1, c='k', transform=ax.transAxes)
    ax.plot(0.06,0.04,"o", ms=4, mew=0.5, markeredgecolor='k', markerfacecolor='None',transform=ax.transAxes)

fig_width = 18/2.54
fig_height = fig_width*0.5
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'normal',
          'font.size': 7,
          'font.weight': 'normal',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 2.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
#sns.set_palette('tab20c')

fig = plt.figure()
gs = fig.add_gridspec(1,2, left=0.05, right=0.97, top=0.97, bottom=0.01, width_ratios=[0.5,1], wspace=0.25)
gs0 = gs[0].subgridspec(2,1, hspace=0.3)
ax0 = fig.add_subplot(gs0[0])

from ctl import plotCtl
plotCtl.run('ctl/co_s.params', ax0, linecolor='slategray')

from orb import plotOrb
ax1 = plotOrb.run('orb', fig, gs[1], lg_posx=1.05)
ax1.text(0.64,0.3,"1", transform=ax1.transAxes, bbox={"boxstyle": "circle", "pad":.2, "facecolor":'w'})
ax1.text(0.64,0.66,"2", transform=ax1.transAxes, bbox={"boxstyle": "circle", "pad":.2, "facecolor":'w'})

gsc = gs0[1].subgridspec(1,2)
axc0 = fig.add_subplot(gsc[0])
axc1 = fig.add_subplot(gsc[1])

ax0.text(-0.15,1.0,'a',fontweight='bold',transform=ax0.transAxes)
ax0.text(1.2,1.0,'b',fontweight='bold',transform=ax0.transAxes)
ax0.text(-0.15,-0.35,'c',fontweight='bold',transform=ax0.transAxes)

plot_png('parchg/PARCHG_625.png', axc0)
plot_png('parchg/PARCHG_626.png', axc1)
axc0.text(0.5,0.9,"1", transform=axc0.transAxes, bbox={"boxstyle": "circle", "pad":.2, "facecolor":'w'})
axc1.text(0.5,0.9,"2", transform=axc1.transAxes, bbox={"boxstyle": "circle", "pad":.2, "facecolor":'w'})
draw_coords(axc0)

plt.savefig('defectlevels.pdf', dpi=600)
