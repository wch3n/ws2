#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches

def parse(params,mu,X,dvbm):
    params = np.genfromtxt(params)
    n_plt = params.shape[1] - 1
    eform_dict = {}
    ctl_dict = {}
    params[:,1] += params[:,0]*dvbm
    print(params)
    for il in range(n_plt):
        eform = np.zeros((len(params), 101))
        for i in range(len(params)):
            eform[i] = list(map(lambda x: (x-X[0])*params[i,0] + params[i,il+1], X))
        eform_min = np.amin(eform, axis=0) + mu
        #
        _c = [params[i,il+1] - params[i+1,il+1] + X[0] for i in range(len(params)-1)]
        _ctl = []
        i = 0
        while i < len(_c):
            if (i == len(_c)-1) or (_c[i] > _c[i+1]):
                _ctl.append(_c[i])
                i += 1
            else:
                _ctl.append(0.5*(_c[i]+_c[i+1]))
                i += 2
        ctl = np.zeros((len(_ctl),2))
        for i,v in enumerate(_ctl):
            ctl[i,0] = v
            ctl[i,1] = eform_min[np.argmin(np.abs(X-v))]
        eform_dict[il] = eform_min
        ctl_dict[il] = ctl
    return eform_dict, ctl_dict

def plot_ctl(x, eform_dict, ctl_dict, color, ax, zorder=9):
    for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], c=color, lw=1.2, solid_capstyle='round', zorder=zorder)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], c=color)

fig_width = 6./2.54
fig_height = fig_width*0.75
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'normal',
          'font.size': 7,
          'font.weight': 'normal',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 2.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'axes.spines.top': True,
          'axes.spines.right': True, 
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

vbm_22 = -6.06
vbm_07 = -5.64

gap_22 = 2.58
gap_07 = 1.95

cbm_22 = vbm_22 + gap_22
cbm_07 = vbm_07 + gap_07
dvb_07 = vbm_22 - vbm_07

yo = 1.2; ye=3.5
x_22 = np.linspace(0,gap_22,101)
patches = []
kwargs = {"fontsize":6}
mu1 = {"S": 0, "Co": 0}
mu2 = {"S": 0, "Co": 0}

def run(params='co_s.params', ax=None, separate_draw=False, linecolor='k'):
    if ax ==  None:
        fig = plt.figure()
        ax = fig.subplots(1,1)
    eform_c_s, ctl_c_s = parse(params, mu1['S'], x_22, dvb_07)
    plot_ctl(x_22, eform_c_s, ctl_c_s, linecolor, ax, zorder=1)
    ax.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(0,gap_22,yo,ye), alpha=0.1, aspect='auto')

    ax.set_xlabel('Fermi level (eV)')
    ax.set_ylabel('Formation energy (eV)')
    ax.set_ylim([yo, ye])
    ax.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(0.4))
    ax.yaxis.set_minor_locator(MultipleLocator(0.2))

    ax.text(2.0,2.8, r'$(0/-)$')
    ax.text(0.3,2.8, r'$(+/0)$')
    ax.text(0.1,0.1, r'$Co_S$', transform=ax.transAxes)

    if separate_draw:
        plt.tight_layout(pad=0.2, h_pad=1.0, w_pad=0.5)
        plt.savefig('ctl.pdf', dpi=300)

if __name__ == '__main__':
    run()
